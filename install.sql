DROP TABLE IF EXISTS gw1_user;
CREATE TABLE gw1_user (
  gwUserID int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  userID int(10) NULL,
  username varchar(255) NOT NULL DEFAULT '',
  gwUsername varchar(255) NOT NULL DEFAULT '',
  alliance varchar(255) NOT NULL DEFAULT '',
  researchPoints int(10) NOT NULL DEFAULT '0',
  planetPoints int(10) NOT NULL DEFAULT '0',
  time int(10) NOT NULL DEFAULT '0'
);

DROP TABLE IF EXISTS gw1_planets;
CREATE TABLE gw1_planets (
  planetID int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gwUserID int(10) NULL,
  username varchar(255) NOT NULL DEFAULT '',
  gwUsername varchar(255) NOT NULL DEFAULT '',
  points int(10) NOT NULL DEFAULT '0',
  time int(10) NOT NULL DEFAULT '0',
  universe tinyint(3) NOT NULL DEFAULT '1',
  solarSystem tinyint(3) NOT NULL DEFAULT '1',
  planet tinyint(2) NOT NULL DEFAULT '1',
  kommandozentrale tinyint(3) NOT NULL DEFAULT '1',
  forschungszentrum tinyint(3) NOT NULL DEFAULT '0',
  eisenmine tinyint(3) NOT NULL DEFAULT '0',
  lutinumraffinerie tinyint(3) NOT NULL DEFAULT '0',
  bohrturm tinyint(3) NOT NULL DEFAULT '0',
  chemiefabrik  tinyint(3) NOT NULL DEFAULT '0',
  erweiterteChemiefabrik tinyint(3) NOT NULL DEFAULT '0',
  eisenspeicher tinyint(3) NOT NULL DEFAULT '0',
  lutinumspeicher tinyint(3) NOT NULL DEFAULT '0',
  wassertanks tinyint(3) NOT NULL DEFAULT '0',
  wasserstofftank tinyint(3) NOT NULL DEFAULT '0',
  schiffsfabrik tinyint(3) NOT NULL DEFAULT '0',
  orbitalenVerteidgungsstation tinyint(3) NOT NULL DEFAULT '0',
  planetarerSchild tinyint(3) NOT NULL DEFAULT '0',
  fusionsreaktor tinyint(3) NOT NULL DEFAULT '0'
);

DROP TABLE IF EXISTS gw1_research;
CREATE TABLE gw1_research (
  researchID int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gwUserID int(10) NULL,
  username varchar(255) NOT NULL DEFAULT '',
  gwUsername varchar(255) NOT NULL DEFAULT '',
  time int(10) NOT NULL DEFAULT '0',
  verbrennungsantrieb tinyint(3) NOT NULL DEFAULT '0',
  ionenantrieb  tinyint(3) NOT NULL DEFAULT '0',
  raumkrümmungsantrieb tinyint(3) NOT NULL DEFAULT '0',
  raumfaltungsantrieb tinyint(3) NOT NULL DEFAULT '0',
  ionisation tinyint(3) NOT NULL DEFAULT '0',
  energiebündelung tinyint(3) NOT NULL DEFAULT '0',
  explosivgeschosse tinyint(3) NOT NULL DEFAULT '0',
  spionageforschung tinyint(3) NOT NULL DEFAULT '0',
  erweiterteSchiffspanzerung tinyint(3) NOT NULL DEFAULT '0',
  erhöhteLadekapazität tinyint(3) NOT NULL DEFAULT '0',
  recyclingTechnik tinyint(3) NOT NULL DEFAULT '0'
);

DROP TABLE IF EXISTS gw1_fleet;
CREATE TABLE gw1_fleet (
  fleetID int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gwUserID int(10) NULL,
  username varchar(255) NOT NULL DEFAULT '',
  gwUsername varchar(255) NOT NULL DEFAULT '',
  planetID int(10) NOT NULL,
  time int(10) NOT NULL DEFAULT '0',
  schakal int(10) NOT NULL DEFAULT '0',
  recycler int(10) NOT NULL DEFAULT '0',
  spionagesonde int(10) NOT NULL DEFAULT '0',
  renegade int(10) NOT NULL DEFAULT '0',
  raider int(10) NOT NULL DEFAULT '0',
  kolonisationsschiff int(10) NOT NULL DEFAULT '0',
  falcon int(10) NOT NULL DEFAULT '0',
  tjuger int(10) NOT NULL DEFAULT '0',
  cougar int(10) NOT NULL DEFAULT '0',
  longeagleV int(10) NOT NULL DEFAULT '0',
  kleinesHandelsschiff int(10) NOT NULL DEFAULT '0',
  großesHandelsschiff int(10) NOT NULL DEFAULT '0',
  noah int(10) NOT NULL DEFAULT '0',
  longeagleX int(10) NOT NULL DEFAULT '0'
);

ALTER TABLE gw1_user ADD FOREIGN KEY (userID) REFERENCES wcf1_user (userID) ON DELETE CASCADE;
ALTER TABLE gw1_planets ADD FOREIGN KEY (gwUserID) REFERENCES gw1_user (gwUserID) ON DELETE CASCADE;
ALTER TABLE gw1_research ADD FOREIGN KEY (gwUserID) REFERENCES gw1_user (gwUserID) ON DELETE CASCADE;

ALTER TABLE gw1_fleet ADD FOREIGN KEY (gwUserID) REFERENCES gw1_user (gwUserID) ON DELETE CASCADE;
ALTER TABLE gw1_fleet ADD FOREIGN KEY (planetID) REFERENCES gw1_planets (planetID) ON DELETE CASCADE;