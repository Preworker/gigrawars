{include file='documentHeader'}

<head>
	<title>{lang}gw.header.planet{/lang} {if $pageNo > 1}- {lang}wcf.page.pageNo{/lang} {/if}- {lang}gw.header.index{/lang} - {PAGE_TITLE|language}</title>
	
	<script data-relocate="true" type="text/javascript">
		//<![CDATA[
		$(function() {
			new WCF.Action.Delete('gw\\data\\planet\\PlanetAction', '.jsPlanetTr');
			
			{event name='javascriptInit'}
		});
		//]]>
	</script>
	
	{include file='headInclude'}
</head>

<body id="tpl{$templateName|ucfirst}" data-template="{$templateName}" data-application="{$templateNameApplication}">

{include file='header'}

<header class="boxHeadline">
	<h1>{lang}gw.header.planet{/lang}{if $items} <span class="badge">{#$items}</span>{/if}</h1>
</header>

{include file='userNotice'}

<div class="contentNavigation">
	{pages print=true assign=pagesLinks application='gw' controller='PlanetList' object=$objects link="pageNo=%d"}
	
	{hascontent}
		<nav>
			<ul>
				{content}
					{if $__wcf->user->userID}<li><a href="{link application='gw' controller='PlanetAdd'}{/link}" title="{lang}gw.planet.planet.add{/lang}" class="button"><span class="icon icon16 icon-asterisk"></span> <span>{lang}gw.planet.planet.add{/lang}</span></a></li>{/if}
					{event name='contentNavigationButtonsTop'}
				{/content}
			</ul>
		</nav>
	{/hascontent}
</div>

{if $items}
	<div class="marginTop tabularBox tabularBoxTitle messageGroupList">
		<table class="table">
			<thead>
				<tr>
					<th class="columnID columnPlanetID{if $sortField == 'planetID'} active {@$sortOrder}{/if}" colspan="2"><a href="{link application='gw' controller='PlanetList'}pageNo={@$pageNo}&sortField=planetID&sortOrder={if $sortField == 'planetID' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}wcf.global.objectID{/lang}</a></th>
					<th class="columnTitle columnUniverse{if $sortField == 'universe'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller='PlanetList'}pageNo={@$pageNo}&sortField=universe&sortOrder={if $sortField == 'universe' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.planet.universe{/lang}</a></th>
					<th class="columnTitle columnSolarSystem{if $sortField == 'solarSystem'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller='PlanetList'}pageNo={@$pageNo}&sortField=solarSystem&sortOrder={if $sortField == 'solarSystem' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.planet.solarSystem{/lang}</a></th>
					<th class="columnTitle columnPlanet{if $sortField == 'planet'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller='PlanetList'}pageNo={@$pageNo}&sortField=planet&sortOrder={if $sortField == 'planet' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.planet.planet{/lang}</a></th>
					<th class="columnTitle columnPoints{if $sortField == 'points'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller='PlanetList'}pageNo={@$pageNo}&sortField=points&sortOrder={if $sortField == 'points' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.planet.points{/lang}</a></th>
					<th class="columnTitle columnPoints{if $sortField == 'time'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller='PlanetList'}pageNo={@$pageNo}&sortField=time&sortOrder={if $sortField == 'time' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.planet.time{/lang}</a></th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$objects item=data}
					<tr class="jsPlanetTr">
						<td class="columnIcon">
							<a href="{link application="gw" controller='FleetEdit' object=$data}{/link}" title="{lang}gw.fleet.edit{/lang}" class="jsTooltip"><span class="icon icon16 fa-space-shuttle"></span></a>
							<a href="{link application="gw" controller='PlanetEdit' object=$data}{/link}" title="{lang}wcf.global.button.edit{/lang}" class="jsTooltip"><span class="icon icon16 icon-pencil"></span></a>
							<span class="icon icon16 icon-remove jsTooltip jsDeleteButton pointer" title="{lang}wcf.global.button.delete{/lang}" data-object-id="{@$data->planetID}" data-confirm-message="{lang}gw.planet.planet.delete.sure{/lang}"></span>
						</td>
						<td class="columnUserID columnID">{#$data->planetID}</td>
						<td class="columnUsername columnText">{$data->universe}</td>
						<td class="columnUsername columnText">{$data->solarSystem}</td>
						<td class="columnUsername columnText">{$data->planet}</td>
						<td class="columnUsername columnText">{#$data->points}</td>
						<td class="columnUsername columnText">{@$data->time|time}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
{else}
	<p class="info">{lang}gw.planet.planet.noOne{/lang}</p>
{/if}

<div class="contentNavigation">
	{@$pagesLinks}
	
	{hascontent}
		<nav>
			<ul>
				{content}
					{if $__wcf->user->userID}<li><a href="{link application='gw' controller='PlanetAdd'}{/link}" title="{lang}gw.planet.planet.add{/lang}" class="button"><span class="icon icon16 icon-asterisk"></span> <span>{lang}gw.planet.planet.add{/lang}</span></a></li>{/if}
					{event name='contentNavigationButtonsBottom'}
				{/content}
			</ul>
		</nav>
	{/hascontent}
</div>

{include file='footer'}

</body>
</html>