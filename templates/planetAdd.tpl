{include file='documentHeader'}

<head>
	<title>{lang}gw.planet.planet.{$action}{/lang} - {PAGE_TITLE|language}</title>
	
	{include file='headInclude'}
	
	<script data-relocate="true">
		//<![CDATA[
		$(function() {
			$("input[type='number']").on('click', function () {
				$(this).select();
			});
			
			$("textarea").on('click', function () {
				$(this).select();
			});
		});
		//]]>
	</script>
</head>

<body id="tpl{$templateName|ucfirst}" data-template="{$templateName}" data-application="{$templateNameApplication}">

{include file='header'}

<header class="boxHeadline">
	<h1>{lang}gw.planet.planet.{$action}{/lang}</h1>
</header>

{include file='userNotice'}

{include file='formError'}

<form id="messageContainer" class="jsFormGuard" method="post" action="{if $action == 'add'}{link application='gw' controller='PlanetAdd'}{/link}{else}{link application='gw' controller='PlanetEdit' object=$planetObject}{/link}{/if}">
	<div class="container containerPadding marginTop">
		
		<fieldset>
			<legend>{lang}gw.planet.cords{/lang}</legend>
			
			<dl{if $errorField == 'universe'} class="formError"{/if}>
				<dt><label for="universe">{lang}gw.planet.universe{/lang}</label></dt>
				<dd>
					<input type="number" id="universe" name="universe" value="{$universe}" class="small" required="required" min="1" max="200" />
					
					{if $errorField == 'universe'}
						<small class="innerError">
							{if $errorType == 'empty'}
								{lang}wcf.global.form.error.empty{/lang}
							{else}
								{lang}gw.error.name.{@$errorType}{/lang}
							{/if}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'solarSystem'} class="formError"{/if}>
				<dt><label for="solarSystem">{lang}gw.planet.solarSystem{/lang}</label></dt>
				<dd>
					<input type="number" id="solarSystem" name="solarSystem" value="{$solarSystem}" class="small" required="required" min="1" max="200" />
					
					{if $errorField == 'solarSystem'}
						<small class="innerError">
							{if $errorType == 'empty'}
								{lang}wcf.global.form.error.empty{/lang}
							{else}
								{lang}gw.error.name.{@$errorType}{/lang}
							{/if}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'planet'} class="formError"{/if}>
				<dt><label for="planet">{lang}gw.planet.planet{/lang}</label></dt>
				<dd>
					<input type="number" id="planet" name="planet" value="{$planet}" class="small" required="required" min="1" max="20" />
					
					{if $errorField == 'planet'}
						<small class="innerError">
							{if $errorType == 'empty'}
								{lang}wcf.global.form.error.empty{/lang}
							{else}
								{lang}gw.error.name.{@$errorType}{/lang}
							{/if}
						</small>
					{/if}
				</dd>
			</dl>
		</fieldset>
		
		<fieldset>
			<legend>{lang}gw.planet.complete{/lang}</legend>
			
			<dl{if $errorField == 'text'} class="formError"{/if}>
				<dt><label for="text">{lang}gw.planet.planet.text{/lang}</label></dt>
				<dd>
					<textarea name="text" id="text" rows="10" cols="40">{$text}</textarea>
					<p>{lang}gw.planet.planet.text.description{/lang}</p>
					
					{if $errorField == 'text'}
						<small class="innerError">
							{if $errorType == 'empty'}
								{lang}wcf.global.form.error.empty{/lang}
							{elseif $errorType == 'censoredWordsFound'}
								{lang}wcf.message.error.censoredWordsFound{/lang}
							{else}
								{lang}gw.error.text.{@$errorType}{/lang}
							{/if}
						</small>
					{/if}
				</dd>
			</dl>
			
			{event name='informationFields'}
		</fieldset>
		
		{if $action == 'edit'}
			<fieldset>
				<legend>{lang}gw.planet.detail{/lang}</legend>
				
				<dl{if $errorField == 'kommandozentrale'} class="formError"{/if}>
					<dt><label for="kommandozentrale">{lang}gw.planet.kommandozentrale{/lang}</label></dt>
					<dd>
						<input type="number" id="kommandozentrale" name="kommandozentrale" value="{$kommandozentrale}" class="small"min="1" max="100" />
						
						{if $errorField == 'kommandozentrale'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'forschungszentrum'} class="formError"{/if}>
					<dt><label for="forschungszentrum">{lang}gw.planet.forschungszentrum{/lang}</label></dt>
					<dd>
						<input type="number" id="forschungszentrum" name="forschungszentrum" value="{$forschungszentrum}" class=" small"min="0" max="100" />
						
						{if $errorField == 'forschungszentrum'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'eisenmine'} class="formError"{/if}>
					<dt><label for="eisenmine">{lang}gw.planet.eisenmine{/lang}</label></dt>
					<dd>
						<input type="number" id="eisenmine" name="eisenmine" value="{$eisenmine}" class=" small"min="0" max="100" />
						
						{if $errorField == 'eisenmine'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'lutinumraffinerie'} class="formError"{/if}>
					<dt><label for="lutinumraffinerie">{lang}gw.planet.lutinumraffinerie{/lang}</label></dt>
					<dd>
						<input type="number" id="lutinumraffinerie" name="lutinumraffinerie" value="{$lutinumraffinerie}" class=" small"min="0" max="100" />
						
						{if $errorField == 'lutinumraffinerie'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'bohrturm'} class="formError"{/if}>
					<dt><label for="bohrturm">{lang}gw.planet.bohrturm{/lang}</label></dt>
					<dd>
						<input type="number" id="bohrturm" name="bohrturm" value="{$bohrturm}" class=" small"min="0" max="100" />
						
						{if $errorField == 'bohrturm'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'chemiefabrik'} class="formError"{/if}>
					<dt><label for="chemiefabrik">{lang}gw.planet.chemiefabrik{/lang}</label></dt>
					<dd>
						<input type="number" id="chemiefabrik" name="chemiefabrik" value="{$chemiefabrik}" class=" small"min="0" max="100" />
						
						{if $errorField == 'chemiefabrik'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'erweiterteChemiefabrik'} class="formError"{/if}>
					<dt><label for="erweiterteChemiefabrik">{lang}gw.planet.erweiterteChemiefabrik{/lang}</label></dt>
					<dd>
						<input type="number" id="erweiterteChemiefabrik" name="erweiterteChemiefabrik" value="{$erweiterteChemiefabrik}" class=" small"min="0" max="100" />
						
						{if $errorField == 'erweiterteChemiefabrik'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'eisenspeicher'} class="formError"{/if}>
					<dt><label for="eisenspeicher">{lang}gw.planet.eisenspeicher{/lang}</label></dt>
					<dd>
						<input type="number" id="eisenspeicher" name="eisenspeicher" value="{$eisenspeicher}" class=" small"min="0" max="100" />
						
						{if $errorField == 'eisenspeicher'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'lutinumspeicher'} class="formError"{/if}>
					<dt><label for="lutinumspeicher">{lang}gw.planet.lutinumspeicher{/lang}</label></dt>
					<dd>
						<input type="number" id="lutinumspeicher" name="lutinumspeicher" value="{$lutinumspeicher}" class=" small"min="0" max="100" />
						
						{if $errorField == 'lutinumspeicher'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'wassertanks'} class="formError"{/if}>
					<dt><label for="wassertanks">{lang}gw.planet.wassertanks{/lang}</label></dt>
					<dd>
						<input type="number" id="wassertanks" name="wassertanks" value="{$wassertanks}" class=" small"min="0" max="100" />
						
						{if $errorField == 'wassertanks'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'wasserstofftank'} class="formError"{/if}>
					<dt><label for="wasserstofftank">{lang}gw.planet.wasserstofftank{/lang}</label></dt>
					<dd>
						<input type="number" id="wasserstofftank" name="wasserstofftank" value="{$wasserstofftank}" class=" small"min="0" max="100" />
						
						{if $errorField == 'wasserstofftank'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'schiffsfabrik'} class="formError"{/if}>
					<dt><label for="schiffsfabrik">{lang}gw.planet.schiffsfabrik{/lang}</label></dt>
					<dd>
						<input type="number" id="schiffsfabrik" name="schiffsfabrik" value="{$schiffsfabrik}" class=" small"min="0" max="100" />
						
						{if $errorField == 'schiffsfabrik'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'orbitalenVerteidgungsstation'} class="formError"{/if}>
					<dt><label for="orbitalenVerteidgungsstation">{lang}gw.planet.orbitalenVerteidgungsstation{/lang}</label></dt>
					<dd>
						<input type="number" id="orbitalenVerteidgungsstation" name="orbitalenVerteidgungsstation" value="{$orbitalenVerteidgungsstation}" class=" small"min="0" max="100" />
						
						{if $errorField == 'orbitalenVerteidgungsstation'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'planetarerSchild'} class="formError"{/if}>
					<dt><label for="planetarerSchild">{lang}gw.planet.planetarerSchild{/lang}</label></dt>
					<dd>
						<input type="number" id="planetarerSchild" name="planetarerSchild" value="{$planetarerSchild}" class=" small"min="0" max="100" />
						
						{if $errorField == 'planetarerSchild'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
				
				<dl{if $errorField == 'fusionsreaktor'} class="formError"{/if}>
					<dt><label for="fusionsreaktor">{lang}gw.planet.fusionsreaktor{/lang}</label></dt>
					<dd>
						<input type="number" id="fusionsreaktor" name="fusionsreaktor" value="{$fusionsreaktor}" class=" small"min="0" max="100" />
						
						{if $errorField == 'fusionsreaktor'}
							<small class="innerError">
								{lang}gw.error.name.{@$errorType}{/lang}
							</small>
						{/if}
					</dd>
				</dl>
			</fieldset>
		{/if}
		{event name='additionalFieldsets'}
	</div>
	
	<div class="formSubmit">
		<input type="submit" value="{lang}wcf.global.button.submit{/lang}" accesskey="s" />
		{@SECURITY_TOKEN_INPUT_TAG}
	</div>
</form>

{include file='footer'}

</body>
</html>