{include file='documentHeader'}

<head>
	<title>{lang}gw.header.research{/lang} - {PAGE_TITLE|language}</title>
	
	{include file='headInclude'}
	
	<script data-relocate="true">
		//<![CDATA[
		$(function() {
			$("input[type='number']").on('click', function () {
				$(this).select();
			});
			
			$("textarea").on('click', function () {
				$(this).select();
			});
		});
		//]]>
	</script>
</head>

<body id="tpl{$templateName|ucfirst}" data-template="{$templateName}" data-application="{$templateNameApplication}">

{include file='header'}

<header class="boxHeadline">
	<h1>{lang}gw.header.research{/lang}</h1>
</header>

{include file='userNotice'}

{include file='formError'}

<form id="messageContainer" class="jsFormGuard" method="post" action="{link application='gw' controller='ResearchEdit' id=$researchObject->gwUserID}{/link}">
	<div class="container containerPadding marginTop">
		
		<fieldset>
			<legend>{lang}gw.planet.complete{/lang}</legend>
			
			<dl{if $errorField == 'text'} class="formError"{/if}>
				<dt><label for="text">{lang}gw.planet.planet.text{/lang}</label></dt>
				<dd>
					<textarea name="text" id="text" rows="10" cols="40">{$text}</textarea>
					<p>{lang}gw.planet.planet.text.description{/lang}</p>
					
					{if $errorField == 'text'}
						<small class="innerError">
							{if $errorType == 'empty'}
								{lang}wcf.global.form.error.empty{/lang}
							{elseif $errorType == 'censoredWordsFound'}
								{lang}wcf.message.error.censoredWordsFound{/lang}
							{else}
								{lang}gw.error.text.{@$errorType}{/lang}
							{/if}
						</small>
					{/if}
				</dd>
			</dl>
			
			{event name='informationFields'}
		</fieldset>
		
		<fieldset>
			<legend>{lang}gw.research.detail{/lang}</legend>
			
			<dl{if $errorField == 'verbrennungsantrieb'} class="formError"{/if}>
				<dt><label for="verbrennungsantrieb">{lang}gw.research.verbrennungsantrieb{/lang}</label></dt>
				<dd>
					<input type="number" id="verbrennungsantrieb" name="verbrennungsantrieb" value="{$verbrennungsantrieb}" class="small"min="0" max="100" />
					
					{if $errorField == 'verbrennungsantrieb'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'ionenantrieb'} class="formError"{/if}>
				<dt><label for="ionenantrieb">{lang}gw.research.ionenantrieb{/lang}</label></dt>
				<dd>
					<input type="number" id="ionenantrieb" name="ionenantrieb" value="{$ionenantrieb}" class="small"min="0" max="100" />
					
					{if $errorField == 'ionenantrieb'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'raumkrümmungsantrieb'} class="formError"{/if}>
				<dt><label for="raumkrümmungsantrieb">{lang}gw.research.raumkrümmungsantrieb{/lang}</label></dt>
				<dd>
					<input type="number" id="raumkrümmungsantrieb" name="raumkrümmungsantrieb" value="{$raumkrümmungsantrieb}" class="small"min="0" max="100" />
					
					{if $errorField == 'raumkrümmungsantrieb'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'raumfaltungsantrieb'} class="formError"{/if}>
				<dt><label for="raumfaltungsantrieb">{lang}gw.research.raumfaltungsantrieb{/lang}</label></dt>
				<dd>
					<input type="number" id="raumfaltungsantrieb" name="raumfaltungsantrieb" value="{$raumfaltungsantrieb}" class="small"min="0" max="100" />
					
					{if $errorField == 'raumfaltungsantrieb'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'ionisation'} class="formError"{/if}>
				<dt><label for="ionisation">{lang}gw.research.ionisation{/lang}</label></dt>
				<dd>
					<input type="number" id="ionisation" name="ionisation" value="{$ionisation}" class="small"min="0" max="100" />
					
					{if $errorField == 'ionisation'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'energiebündelung'} class="formError"{/if}>
				<dt><label for="energiebündelung">{lang}gw.research.energiebündelung{/lang}</label></dt>
				<dd>
					<input type="number" id="energiebündelung" name="energiebündelung" value="{$energiebündelung}" class="small"min="0" max="100" />
					
					{if $errorField == 'energiebündelung'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'explosivgeschosse'} class="formError"{/if}>
				<dt><label for="explosivgeschosse">{lang}gw.research.explosivgeschosse{/lang}</label></dt>
				<dd>
					<input type="number" id="explosivgeschosse" name="explosivgeschosse" value="{$explosivgeschosse}" class="small"min="0" max="100" />
					
					{if $errorField == 'explosivgeschosse'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'spionageforschung'} class="formError"{/if}>
				<dt><label for="spionageforschung">{lang}gw.research.spionageforschung{/lang}</label></dt>
				<dd>
					<input type="number" id="spionageforschung" name="spionageforschung" value="{$spionageforschung}" class="small"min="0" max="100" />
					
					{if $errorField == 'spionageforschung'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'erweiterteSchiffspanzerung'} class="formError"{/if}>
				<dt><label for="erweiterteSchiffspanzerung">{lang}gw.research.erweiterteSchiffspanzerung{/lang}</label></dt>
				<dd>
					<input type="number" id="erweiterteSchiffspanzerung" name="erweiterteSchiffspanzerung" value="{$erweiterteSchiffspanzerung}" class="small"min="0" max="100" />
					
					{if $errorField == 'erweiterteSchiffspanzerung'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'erhöhteLadekapazität'} class="formError"{/if}>
				<dt><label for="erhöhteLadekapazität">{lang}gw.research.erhöhteLadekapazität{/lang}</label></dt>
				<dd>
					<input type="number" id="erhöhteLadekapazität" name="erhöhteLadekapazität" value="{$erhöhteLadekapazität}" class="small"min="0" max="100" />
					
					{if $errorField == 'erhöhteLadekapazität'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'recyclingTechnik'} class="formError"{/if}>
				<dt><label for="recyclingTechnik">{lang}gw.research.recyclingTechnik{/lang}</label></dt>
				<dd>
					<input type="number" id="recyclingTechnik" name="recyclingTechnik" value="{$recyclingTechnik}" class="small"min="0" max="100" />
					
					{if $errorField == 'recyclingTechnik'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
		</fieldset>
		{event name='additionalFieldsets'}
	</div>
	
	<div class="formSubmit">
		<input type="submit" value="{lang}wcf.global.button.submit{/lang}" accesskey="s" />
		{@SECURITY_TOKEN_INPUT_TAG}
	</div>
</form>

{include file='footer'}

</body>
</html>