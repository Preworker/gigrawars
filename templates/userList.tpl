{include file='documentHeader'}

<head>
	<title>{lang}gw.header.{$action}{/lang} {if $pageNo > 1}- {lang}wcf.page.pageNo{/lang} {/if}- {lang}gw.header.index{/lang} - {PAGE_TITLE|language}</title>
	
	<script data-relocate="true" type="text/javascript">
		//<![CDATA[
		$(function() {
			{if $__wcf->session->getPermission('user.gw.general.canDeleteUsers')}new WCF.Action.Delete('gw\\data\\user\\UserAction', '.jsUserTr');{/if}
			
			{event name='javascriptInit'}
		});
		//]]>
	</script>
	
	{include file='headInclude'}
</head>

<body id="tpl{$templateName|ucfirst}" data-template="{$templateName}" data-application="{$templateNameApplication}">

{include file='header'}

<header class="boxHeadline">
	<h1>{lang}gw.header.{$action}{/lang}{if $items} <span class="badge">{#$items}</span>{/if}</h1>
</header>

{include file='userNotice'}

<div class="contentNavigation">
	{if $action == 'alliance'}
		{pages print=true assign=pagesLinks application='gw' controller='UserList' object=$objects link="pageNo=%d"}	{pages print=true assign=pagesLinks application='gw' controller='UserList' object=$objects link="pageNo=%d"}
		{assign var=controller value='UserList'}
	{else}
		{pages print=true assign=pagesLinks application='gw' controller='UserList' object=$objects link="pageNo=%d"}
		{assign var=controller value='AllUserList'}
	{/if}
</div>

{if $items}
	<div class="marginTop tabularBox tabularBoxTitle messageGroupList">
		<table class="table">
			<thead>
				<tr>
					<th class="columnID columnGwUserID{if $sortField == 'gwUserID'} active {@$sortOrder}{/if}" colspan="2"><a href="{link application='gw' controller=$controller}pageNo={@$pageNo}&sortField=gwUserID&sortOrder={if $sortField == 'gwUserID' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}wcf.global.objectID{/lang}</a></th>
					<th class="columnTitle columnUsername{if $sortField == 'username'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller=$controller}pageNo={@$pageNo}&sortField=username&sortOrder={if $sortField == 'username' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.user.username{/lang}</a></th>
					<th class="columnTitle columnGwUsername{if $sortField == 'gwUsername'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller=$controller}pageNo={@$pageNo}&sortField=gwUsername&sortOrder={if $sortField == 'gwUsername' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.user.gwUsername{/lang}</a></th>
					<th class="columnTitle columnPlanetPoints{if $sortField == 'planetPoints'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller=$controller}pageNo={@$pageNo}&sortField=planetPoints&sortOrder={if $sortField == 'planetPoints' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.user.planetPoints{/lang}</a></th>
					<th class="columnTitle columnResearchPoints{if $sortField == 'researchPoints'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller=$controller}pageNo={@$pageNo}&sortField=researchPoints&sortOrder={if $sortField == 'researchPoints' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.user.researchPoints{/lang}</a></th>
					<th class="columnTitle columnTotalPoints{if $sortField == 'totalPoints'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller=$controller}pageNo={@$pageNo}&sortField=totalPoints&sortOrder={if $sortField == 'totalPoints' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.user.totalPoints{/lang}</a></th>
					<th class="columnTitle columnTime{if $sortField == 'time'} active {@$sortOrder}{/if}"><a href="{link application='gw' controller=$controller}pageNo={@$pageNo}&sortField=time&sortOrder={if $sortField == 'time' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}gw.user.time{/lang}</a></th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$objects item=data}
					<tr class="jsUserTr">
						<td class="columnIcon">{if $__wcf->session->getPermission('user.gw.general.canDeleteUsers')}<span class="icon icon16 icon-remove jsTooltip jsDeleteButton pointer" title="{lang}wcf.global.button.delete{/lang}" data-object-id="{@$data->gwUserID}" data-confirm-message="{lang}gw.user.delete.sure{/lang}"></span>{/if}</td>
						<td class="columnUserID columnID">{#$data->userID}</td>
						<td class="columnUsername columnText"><a href="{link application='gw' controller='User' object=$data}{/link}">{$data->username}</a></td>
						<td class="columnUsername columnText"><a href="{link application='gw' controller='User' object=$data}{/link}">{$data->gwUsername}</a></td>
						<td class="columnUsername columnText">{#$data->planetPoints}</td>
						<td class="columnUsername columnText">{#$data->researchPoints}</td>
						<td class="columnUsername columnText">{#$data->totalPoints}</td>
						<td class="columnUsername columnText">{@$data->getLastUpdateTime()|time}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
{else}
	<p class="info">{lang}gw.user.noOne{/lang}</p>
{/if}

<div class="contentNavigation">
	{@$pagesLinks}
</div>

{include file='footer'}

</body>
</html>