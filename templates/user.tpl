{include file='documentHeader'}

<head>
	<title>{lang}{$user->username}{/lang} - {lang}gw.header.index{/lang} - {PAGE_TITLE|language}</title>
	
	{include file='headInclude'}
	
	<script data-relocate="true">
		//<![CDATA[
		$(function() {
			WCF.TabMenu.init();
		});
		//]]>
	</script>
</head>

<body id="tpl{$templateName|ucfirst}" data-template="{$templateName}" data-application="{$templateNameApplication}">

{include file='header'}

<header class="boxHeadline">
	<h1>{$user->username}</h1>
</header>

{include file='userNotice'}

<div class="wcfShopListContent marginTop tabMenuContainer">
	<nav class="tabMenu">
		<ul>
			<li><a href="{@$__wcf->getAnchor('research')}"><span>{lang}gw.header.research{/lang}</span></a></li>
			{foreach from=$planets item=planet}
				{assign var=anchor value="planet"|concat:$planet->planetID}
				<li><a href="{@$__wcf->getAnchor($anchor)}"><span>{$planet->universe}:{$planet->solarSystem}:{$planet->planet}</span></a></li>
			{/foreach}
		</ul>
	</nav>
	
	{* research *}
	<div class="container containerPadding tabMenuContent" id="research">
		<div class="tabularBox">
			<table class="table">
				<thead>
					<tr class="tableHead">
						<th class="columnID columnResearch">{lang}gw.planet.name{/lang}</th>
						<th class="columnTitle columnLevel">{lang}gw.planet.level{/lang}</th>
					</tr>
				</thead>
				<tbody>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.verbrennungsantrieb{/lang}</td>
						<td class="columnUsername columnText">{#$research->verbrennungsantrieb}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.ionenantrieb{/lang}</td>
						<td class="columnUsername columnText">{#$research->ionenantrieb}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.raumkrümmungsantrieb{/lang}</td>
						<td class="columnUsername columnText">{#$research->raumkrümmungsantrieb}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.raumfaltungsantrieb{/lang}</td>
						<td class="columnUsername columnText">{#$research->raumfaltungsantrieb}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.ionisation{/lang}</td>
						<td class="columnUsername columnText">{#$research->ionisation}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.energiebündelung{/lang}</td>
						<td class="columnUsername columnText">{#$research->energiebündelung}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.explosivgeschosse{/lang}</td>
						<td class="columnUsername columnText">{#$research->explosivgeschosse}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.spionageforschung{/lang}</td>
						<td class="columnUsername columnText">{#$research->spionageforschung}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.erweiterteSchiffspanzerung{/lang}</td>
						<td class="columnUsername columnText">{#$research->erweiterteSchiffspanzerung}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.erhöhteLadekapazität{/lang}</td>
						<td class="columnUsername columnText">{#$research->erhöhteLadekapazität}</td>
					</tr>
					<tr class="jsResearchTr">
						<td class="columnUsername columnText">{lang}gw.research.recyclingTechnik{/lang}</td>
						<td class="columnUsername columnText">{#$research->recyclingTechnik}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	{* planets *}
	{foreach from=$planets item=planet}
		{assign var=anchor value="planet"|concat:$planet->planetID}
		{assign var=fleet value=$planet->getFleet()}
		<div class="container containerPadding tabMenuContent" id="{$anchor}">
			<div class="tabularBox">
				<table class="table">
					<thead>
						<tr class="tableHead">
							<th class="columnID columnName">{lang}gw.planet.name{/lang}</th>
							<th class="columnTitle columnLevel">{lang}gw.planet.level{/lang}</th>
						</tr>
					</thead>
					<tbody>
						<tr class="jsFleetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.schakal{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->schakal}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.recycler{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->recycler}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.spionagesonde{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->spionagesonde}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.renegade{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->renegade}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.raider{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->raider}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.falcon{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->falcon}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.tjuger{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->tjuger}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.cougar{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->cougar}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.longeagleV{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->longeagleV}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.kleinesHandelsschiff{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->kleinesHandelsschiff}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.großesHandelsschiff{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->großesHandelsschiff}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.noah{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->noah}</td>
						</tr><tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.fleet.longeagleX{/lang}</td>
							<td class="columnUsername columnText">{#$fleet->longeagleX}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="tabularBox marginTop">
				<table class="table">
					<thead>
						<tr class="tableHead">
							<th class="columnID columnName">{lang}gw.planet.name{/lang}</th>
							<th class="columnTitle columnLevel">{lang}gw.planet.level{/lang}</th>
						</tr>
					</thead>
					<tbody>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.kommandozentrale{/lang}</td>
							<td class="columnUsername columnText">{#$planet->kommandozentrale}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.forschungszentrum{/lang}</td>
							<td class="columnUsername columnText">{#$planet->forschungszentrum}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.eisenmine{/lang}</td>
							<td class="columnUsername columnText">{#$planet->eisenmine}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.lutinumraffinerie{/lang}</td>
							<td class="columnUsername columnText">{#$planet->lutinumraffinerie}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.bohrturm{/lang}</td>
							<td class="columnUsername columnText">{#$planet->bohrturm}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.chemiefabrik{/lang}</td>
							<td class="columnUsername columnText">{#$planet->chemiefabrik}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.erweiterteChemiefabrik{/lang}</td>
							<td class="columnUsername columnText">{#$planet->erweiterteChemiefabrik}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.eisenspeicher{/lang}</td>
							<td class="columnUsername columnText">{#$planet->eisenspeicher}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.lutinumspeicher{/lang}</td>
							<td class="columnUsername columnText">{#$planet->lutinumspeicher}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.wassertanks{/lang}</td>
							<td class="columnUsername columnText">{#$planet->wassertanks}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.wasserstofftank{/lang}</td>
							<td class="columnUsername columnText">{#$planet->wasserstofftank}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.schiffsfabrik{/lang}</td>
							<td class="columnUsername columnText">{#$planet->schiffsfabrik}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.orbitalenVerteidgungsstation{/lang}</td>
							<td class="columnUsername columnText">{#$planet->orbitalenVerteidgungsstation}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.planetarerSchild{/lang}</td>
							<td class="columnUsername columnText">{#$planet->planetarerSchild}</td>
						</tr>
						<tr class="jsPlanetTr">
							<td class="columnUsername columnText">{lang}gw.planet.fusionsreaktor{/lang}</td>
							<td class="columnUsername columnText">{#$planet->fusionsreaktor}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	{/foreach}
</div>

{include file='footer'}

</body>
</html>