{include file='documentHeader'}

<head>
	<title>{lang}gw.fleet.edit{/lang} - {PAGE_TITLE|language}</title>
	
	{include file='headInclude'}
	
	<script data-relocate="true">
		//<![CDATA[
		$(function() {
			$("input[type='number']").on('click', function () {
				$(this).select();
			});
			
			$("textarea").on('click', function () {
				$(this).select();
			});
		});
		//]]>
	</script>
</head>

<body id="tpl{$templateName|ucfirst}" data-template="{$templateName}" data-application="{$templateNameApplication}">

{include file='header'}

<header class="boxHeadline">
	<h1>{lang}gw.fleet.edit{/lang}</h1>
</header>

{include file='userNotice'}

{include file='formError'}

<form id="messageContainer" class="jsFormGuard" method="post" action="{link application='gw' controller='FleetEdit' object=$fleetObject->getPlanet()}{/link}">
	<div class="container containerPadding marginTop">
		
		<fieldset>
			<legend>{lang}gw.planet.complete{/lang}</legend>
			
			<dl{if $errorField == 'text'} class="formError"{/if}>
				<dt><label for="text">{lang}gw.planet.planet.text{/lang}</label></dt>
				<dd>
					<textarea name="text" id="text" rows="10" cols="40">{$text}</textarea>
					<p>{lang}gw.planet.planet.text.description{/lang}</p>
					
					{if $errorField == 'text'}
						<small class="innerError">
							{if $errorType == 'empty'}
								{lang}wcf.global.form.error.empty{/lang}
							{elseif $errorType == 'censoredWordsFound'}
								{lang}wcf.message.error.censoredWordsFound{/lang}
							{else}
								{lang}gw.error.text.{@$errorType}{/lang}
							{/if}
						</small>
					{/if}
				</dd>
			</dl>
			
			{event name='informationFields'}
		</fieldset>
		
		<fieldset>
			<legend>{lang}gw.fleet.detail{/lang}</legend>
			
			<dl{if $errorField == 'schakal'} class="formError"{/if}>
				<dt><label for="schakal">{lang}gw.fleet.schakal{/lang}</label></dt>
				<dd>
					<input type="number" id="schakal" name="schakal" value="{$schakal}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'schakal'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'recycler'} class="formError"{/if}>
				<dt><label for="recycler">{lang}gw.fleet.recycler{/lang}</label></dt>
				<dd>
					<input type="number" id="recycler" name="recycler" value="{$recycler}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'recycler'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'spionagesonde'} class="formError"{/if}>
				<dt><label for="spionagesonde">{lang}gw.fleet.spionagesonde{/lang}</label></dt>
				<dd>
					<input type="number" id="spionagesonde" name="spionagesonde" value="{$spionagesonde}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'spionagesonde'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'renegade'} class="formError"{/if}>
				<dt><label for="renegade">{lang}gw.fleet.renegade{/lang}</label></dt>
				<dd>
					<input type="number" id="renegade" name="renegade" value="{$renegade}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'renegade'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'raider'} class="formError"{/if}>
				<dt><label for="raider">{lang}gw.fleet.raider{/lang}</label></dt>
				<dd>
					<input type="number" id="raider" name="raider" value="{$raider}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'raider'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'falcon'} class="formError"{/if}>
				<dt><label for="falcon">{lang}gw.fleet.falcon{/lang}</label></dt>
				<dd>
					<input type="number" id="falcon" name="falcon" value="{$falcon}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'falcon'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'tjuger'} class="formError"{/if}>
				<dt><label for="tjuger">{lang}gw.fleet.tjuger{/lang}</label></dt>
				<dd>
					<input type="number" id="tjuger" name="tjuger" value="{$tjuger}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'tjuger'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'kolonisationsschiff'} class="formError"{/if}>
				<dt><label for="kolonisationsschiff">{lang}gw.fleet.kolonisationsschiff{/lang}</label></dt>
				<dd>
					<input type="number" id="kolonisationsschiff" name="kolonisationsschiff" value="{$kolonisationsschiff}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'kolonisationsschiff'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'cougar'} class="formError"{/if}>
				<dt><label for="cougar">{lang}gw.fleet.cougar{/lang}</label></dt>
				<dd>
					<input type="number" id="cougar" name="cougar" value="{$cougar}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'cougar'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'longeagleV'} class="formError"{/if}>
				<dt><label for="longeagleV">{lang}gw.fleet.longeagleV{/lang}</label></dt>
				<dd>
					<input type="number" id="longeagleV" name="longeagleV" value="{$longeagleV}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'longeagleV'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'kleinesHandelsschiff'} class="formError"{/if}>
				<dt><label for="kleinesHandelsschiff">{lang}gw.fleet.kleinesHandelsschiff{/lang}</label></dt>
				<dd>
					<input type="number" id="kleinesHandelsschiff" name="kleinesHandelsschiff" value="{$kleinesHandelsschiff}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'kleinesHandelsschiff'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'großesHandelsschiff'} class="formError"{/if}>
				<dt><label for="großesHandelsschiff">{lang}gw.fleet.großesHandelsschiff{/lang}</label></dt>
				<dd>
					<input type="number" id="großesHandelsschiff" name="großesHandelsschiff" value="{$großesHandelsschiff}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'großesHandelsschiff'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'noah'} class="formError"{/if}>
				<dt><label for="noah">{lang}gw.fleet.noah{/lang}</label></dt>
				<dd>
					<input type="number" id="noah" name="noah" value="{$noah}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'noah'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
			<dl{if $errorField == 'longeagleX'} class="formError"{/if}>
				<dt><label for="longeagleX">{lang}gw.fleet.longeagleX{/lang}</label></dt>
				<dd>
					<input type="number" id="longeagleX" name="longeagleX" value="{$longeagleX}" class="small"min="0" max="1000000" />
					
					{if $errorField == 'longeagleX'}
						<small class="innerError">
							{lang}gw.error.name.{@$errorType}{/lang}
						</small>
					{/if}
				</dd>
			</dl>
			
		</fieldset>
		{event name='additionalFieldsets'}
	</div>
	
	<div class="formSubmit">
		<input type="submit" value="{lang}wcf.global.button.submit{/lang}" accesskey="s" />
		{@SECURITY_TOKEN_INPUT_TAG}
	</div>
</form>

{include file='footer'}

</body>
</html>