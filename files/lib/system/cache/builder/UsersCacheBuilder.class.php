<?php
namespace gw\system\cache\builder;
use wcf\system\cache\builder\AbstractCacheBuilder;
use wcf\data\user\User;
use wcf\system\WCF;

/**
 * Create the cache for the users which can use the alli manager
 *
 * @author 	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.multihunter2
 */
class UsersCacheBuilder extends AbstractCacheBuilder {
	/**
	 * @see	wcf\system\cache\builder\AbstractCacheBuilder::rebuild()
	 */
	public function rebuild(array $parameters) {
		$users = array();
		
		$sql = "SELECT		user.userID
				FROM		wcf".WCF_N."_user_group_option groupOption
				LEFT JOIN	wcf".WCF_N."_user_group_option_value groupValue
					ON(groupValue.optionID = groupOption.optionID)
				LEFT JOIN	wcf".WCF_N."_user_to_group user
					ON(user.groupID = groupValue.groupID)
				WHERE	groupOption.optionName = 'user.profile.multi.getNotifications'
					AND	groupValue.optionValue = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array(1));
		while ($row = $statement->fetchArray()) {
			$users[$row['userID']] = new User($row['userID']);
		}
		
		return $users;
	}
}