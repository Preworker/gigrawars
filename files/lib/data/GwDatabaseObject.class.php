<?php
namespace gw\data;
use wcf\data\DatabaseObject;

/**
 * Abstract class for Clan data holder classes.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
abstract class GwDatabaseObject extends DatabaseObject {
	/**
	 * @see	wcf\data\IStorableObject::getDatabaseTableName()
	 */
	public static function getDatabaseTableName() {
		return 'gw'.WCF_N.'_'.static::$databaseTableName;
	}
}
