<?php
namespace gw\data\planet;
use gw\data\fleet\Fleet;
use gw\data\GwDatabaseObject;
use wcf\system\request\IRouteController;
use wcf\system\WCF;

/**
 * Represents a planet.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class Planet extends GwDatabaseObject implements IRouteController {
	/**
	 * @see	wcf\data\DatabaseObject::$databaseTableName
	 */
	protected static $databaseTableName = 'planets';
	
	/**
	 * @see	wcf\data\DatabaseObject::$databaseTableIndexName
	 */
	protected static $databaseTableIndexName = 'planetID';
	
	/**
	 * @see	\wcf\system\request\IRouteController::getTitle()
	 */
	public function getTitle() {
		return $this->universe.':'.$this->solarSystem.':'.$this->planet;
	}
	
	/**
	 * Get the fleet of the planet
	 */
	public function getFleet() {
		return new Fleet(null, null, null, $this->planetID);
	}
}
