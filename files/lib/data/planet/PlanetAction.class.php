<?php
namespace gw\data\planet;
use gw\data\user\User;
use gw\data\user\UserAction;
use wcf\data\AbstractDatabaseObjectAction;
use wcf\system\exception\UserInputException;
use wcf\system\WCF;

/**
 * Executes planet-related actions.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class PlanetAction extends AbstractDatabaseObjectAction {
	/**
	 * @see	wcf\data\AbstractDatabaseObjectAction::$className
	 */
	protected $className = 'gw\data\planet\PlanetEditor';
	
	/**
	 * @see wcf\data\AbstractDatabaseObjectAction::create()
	 */
	public function create() {
		$gwUser = User::checkUserID(WCF::getUser()->userID);
		if($gwUser === null) {
			$data = array(
				'userID' => $this->parameters['data']['userID'],
				'username' => $this->parameters['data']['username'],
				'gwUsername' => $this->parameters['data']['gwUsername']
			);
			
			$objectData = array(
				'data' => $data
			);
			
			$objectAction = new UserAction(array(), 'create', $objectData);
			$resultValues = $objectAction->executeAction();
			$returnValues = $objectAction->getReturnValues();
			
			$this->parameters['data']['gwUserID'] = $returnValues['returnValues']->gwUserID;
		}
		
		unset($this->parameters['data']['userID']);
		
		$planet = parent::create();
		
		return $planet;
	}
	
	/**
	 * Validates permissions and parameters.
	 */
	public function validateDelete() {
		// read objects
		if (!count($this->objects)) {
			$this->readObjects();
		}
		
		foreach ($this->objects as $object) {
			if($object->userID != WCF::getUser()->userID) {
				throw new UserInputException("Not youre planet!");
			}
		}
	}
}
