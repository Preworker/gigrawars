<?php
namespace gw\data\planet;
use wcf\data\DatabaseObjectList;

/**
 * Represents a list of planets.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class PlanetList extends DatabaseObjectList {
	/**
	 * @see	wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'gw\data\planet\Planet';
}
