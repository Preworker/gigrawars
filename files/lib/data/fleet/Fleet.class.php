<?php
namespace gw\data\fleet;
use gw\data\planet\Planet;
use gw\data\GwDatabaseObject;
use wcf\system\request\IRouteController;
use wcf\system\WCF;

/**
 * Represents a fleet.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class Fleet extends GwDatabaseObject implements IRouteController {
	/**
	 * @see	wcf\data\DatabaseObject::$databaseTableName
	 */
	protected static $databaseTableName = 'fleet';
	
	/**
	 * @see	wcf\data\DatabaseObject::$databaseTableIndexName
	 */
	protected static $databaseTableIndexName = 'fleetID';
	
	/**
	 * planet object
	 */
	public $planet = null;
	
	/**
	 * Creates a new instance of the DatabaseObject class.
	 */
	public function __construct($id, array $row = null, DatabaseObject $object = null, $planetID = null) {
		if ($id !== null) {
			$sql = "SELECT	*
				FROM	".static::getDatabaseTableName()." fleet
				WHERE	".static::getDatabaseTableIndexName()." = ?";
			$statement = WCF::getDB()->prepareStatement($sql);
			$statement->execute(array($id));
			$row = $statement->fetchArray();
			
			// enforce data type 'array'
			if ($row === false) $row = array();
		}
		else if ($planetID !== null) {
			$sql = "SELECT	*
				FROM	".static::getDatabaseTableName()." fleet
				WHERE	planetID = ?";
			$statement = WCF::getDB()->prepareStatement($sql);
			$statement->execute(array($planetID));
			$row = $statement->fetchArray();
			
			// enforce data type 'array'
			if ($row === false) $row = array();
		}
		else if ($object !== null) {
			$row = $object->data;
		}
		
		$this->handleData($row);
	}
	
	/**
	 * @see	\wcf\system\request\IRouteController::getTitle()
	 */
	public function getTitle() {
		return $this->getPlanet()->universe.':'.$this->getPlanet()->solarSystem.':'.$this->getPlanet()->planet;
	}
	
	/**
	 * return a planet object
	 */
	public function getPlanet() {
		if(!$this->planet) {
			$this->planet = new Planet($this->planetID);
		}
		
		return $this->planet;
	}
}
