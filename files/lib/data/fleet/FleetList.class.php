<?php
namespace gw\data\fleet;
use wcf\data\DatabaseObjectList;

/**
 * Represents a list of fleets.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class FleetList extends DatabaseObjectList {
	/**
	 * @see	wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'gw\data\fleet\Fleet';
}
