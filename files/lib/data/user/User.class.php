<?php
namespace gw\data\user;
use gw\data\planet\Planet;
use gw\data\research\Research;
use gw\data\GwDatabaseObject;
use wcf\system\request\IRouteController;
use wcf\system\WCF;

/**
 * Represents a user.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class User extends GwDatabaseObject implements IRouteController {
	/**
	 * @see	wcf\data\DatabaseObject::$databaseTableName
	 */
	protected static $databaseTableName = 'user';
	
	/**
	 * @see	wcf\data\DatabaseObject::$databaseTableIndexName
	 */
	protected static $databaseTableIndexName = 'gwUserID';
	
	/**
	 * @see	\wcf\system\request\IRouteController::getTitle()
	 */
	public function getTitle() {
		return $this->username;
	}
	
	/**
	 * check if given user ID exist
	 */
	public static function checkUserID($userID) {
		$sql = "SELECT	*
				FROM	gw".WCF_N."_user
				WHERE userID = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array(
			$userID
		));
		$row = $statement->fetchArray();
		
		if($row !== false) {
			$user = new User(null, $row);
			if($user->gwUserID) {
				return $user;
			}
		}
		
		return null;
	}
	
	/**
	 * get the last update time
	 */
	public function getLastUpdateTime() {
		$sql = "SELECT	time AS planetTime
				FROM	gw".WCF_N."_planets
				WHERE gwUserID = ?
				ORDER BY time DESC";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array(
			$this->gwUserID
		));
		$planetTime = $statement->fetchArray();
		
		$sql = "SELECT	time AS researchTime
				FROM	gw".WCF_N."_research
				WHERE gwUserID = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array(
			$this->gwUserID
		));
		$researchTime = $statement->fetchArray();
		
		$sql = "SELECT	time AS fleetTime
				FROM	gw".WCF_N."_fleet
				WHERE gwUserID = ?
				ORDER BY time DESC";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array(
			$this->gwUserID
		));
		$fleetTime = $statement->fetchArray();
		
		return max($planetTime['planetTime'], $researchTime['researchTime'], $fleetTime['fleetTime']);
	}
	
	/**
	 * Get all planets of the user
	 */
	public function getPlanets() {
		$planets = array();
		
		$sql = "SELECT	*
				FROM	gw".WCF_N."_planets
				WHERE	gwUserID = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array(
			$this->gwUserID
		));
		while ($row = $statement->fetchArray()) {
			$planets[] = new Planet(null, $row);
		}
		
		return $planets;
	}
	
	/**
	 * Get the research of the user
	 */
	public function getResearch() {
		return new Research(null, null, null, $this->gwUserID);
	}
}
