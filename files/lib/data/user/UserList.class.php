<?php
namespace gw\data\user;
use gw\system\cache\builder\UsersCacheBuilder;
use wcf\data\DatabaseObjectList;
use wcf\system\WCF;

/**
 * Represents a list of users.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class UserList extends DatabaseObjectList {
	/**
	 * @see	wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'gw\data\user\User';
	
	/**
	 * @see	wcf\data\DatabaseObjectList::__construct
	 */
	public function __construct() {
		parent::__construct();
		// get like status
		if (!empty($this->sqlSelects)) $this->sqlSelects .= ',';
		$this->sqlSelects .= "(researchPoints + planetPoints) AS totalPoints";
	}
}
