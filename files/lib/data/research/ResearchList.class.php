<?php
namespace gw\data\research;
use gw\system\cache\builder\UsersCacheBuilder;
use wcf\data\DatabaseObjectList;
use wcf\system\WCF;

/**
 * Represents a list of researches.
 * 
 * @author	Marcel Beckers
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	de.yourecom.gw
 */
class ResearchList extends DatabaseObjectList {
	/**
	 * @see	wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'gw\data\research\Research';
	
	public function __construct() {
		parent::__construct();
		
		$userCache = UsersCacheBuilder::getInstance()->getData();
		
		if (!WCF::getSession()->getPermission('user.gw.general.canSeeAll')) {
			$this->getConditionBuilder()->add('research.userID NOT IN ?', array_keys($userCache));
		}
	}
}
