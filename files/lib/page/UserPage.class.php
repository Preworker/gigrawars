<?php 
namespace gw\page;
use gw\data\user\User;
use wcf\page\AbstractPage;
use wcf\system\exception\IllegalLinkException;
use wcf\system\breadcrumb\Breadcrumb;
use wcf\system\request\LinkHandler;
use wcf\system\WCF;

/**
 * Shows a list of treasure entries.
 * 
 * @author	Marcel Beckers
 */
class UserPage extends AbstractPage {
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $activeMenuItem = 'gw.header.menu.index';
	
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $loginRequired = true;
	
	/**
	 * @see	wcf\page\AbstractPage::$neededPermissions
	 */
	public $neededPermissions = array('user.gw.general.canSeeAlliance');
	
	/**
	 * user ID
	 */
	public $userID = 0;
	
	/**
	 * @see	wcf\page\IPage::readParameters()
	 */
	public function readParameters() {
		if (!empty($_REQUEST['id'])) {
			$this->userID = intval($_REQUEST['id']);
		}
		
		parent::readParameters();
	}
	
	/**
	 * @see	wcf\page\IPage::readData()
	 */
	public function readData() {
		parent::readData();
		
		$this->userObject = new User($this->userID);
		if (!$this->userObject->gwUserID) {
			throw new IllegalLinkException();
		}
		
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.index'), LinkHandler::getInstance()->getLink('GwIndex', array(
			'application' => 'gw'
		))));
		
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.alliance'), LinkHandler::getInstance()->getLink('UserList', array(
			'application' => 'gw'
		))));
	}
	
	/**
	 * @see	wcf\page\IPage::assignVariables()
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		WCF::getTPL()->assign(array(
			'user' => $this->userObject,
			'research' => $this->userObject->getResearch(),
			'planets' => $this->userObject->getPlanets(),
			'allowSpidersToIndexThisPage' => true
		));
	}
}
