<?php
namespace gw\form;
use gw\data\fleet\Fleet;
use gw\data\fleet\FleetAction;
use wcf\form\AbstractForm;
use wcf\system\exception\UserInputException;
use wcf\util\StringUtil;
use wcf\system\breadcrumb\Breadcrumb;
use wcf\system\request\LinkHandler;
use wcf\util\HeaderUtil;
use wcf\system\WCF;

/**
 * Shows the fleet edit form.
 * 
 * @author	Marcel Beckers
 * @license	YoureCom License - Commercial (YCLC)  <http://yourecom.de/hp/index.php?licence-commercial/>
 * @package	de.yourecom.cbs
 */
class FleetEditForm extends AbstractForm {
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $activeMenuItem = 'gw.header.index';
	
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $loginRequired = true;
	
	/**
	 * @see	wcf\page\AbstractPage::$neededPermissions
	 */
	public $neededPermissions = array('user.gw.general.cannUse');
	
	/**
	 * name of the template for the called page
	 * @var	string
	 */
	public $templateName = 'fleetEdit';
	
	/**
	 * planet ID
	 * @var	integer
	 */
	public $planetID = 0;
	
	/**
	 * fleet object
	 * @var	gw\data\fleet\Fleet
	 */
	public $fleetObject = null;
	
	/**
	 * text
	 * @var	string
	 */
	public $text = '';
	
	/**
	 * @see	wcf\page\IPage::readParameters()
	 */
	public function readParameters() {
		if (!empty($_REQUEST['id'])) {
			$this->planetID = intval($_REQUEST['id']);
			$this->fleetObject = new Fleet(null, null, null, $this->planetID);
		}
		//if dont exist a row create a new one
		if (!$this->fleetObject->fleetID) {
			$data = array(
				'gwUserID' => WCF::getUser()->userID,
				'username' => WCF::getUser()->username,
				'gwUsername' => WCF::getUser()->username,
				'planetID' => $this->planetID,
				'alliance' => GW_ALLIANCE_TAG
			);
			
			$objectData = array(
				'data' => $data
			);
			
			$this->objectAction = new FleetAction(array(), 'create', $objectData);
			$resultValues = $this->objectAction->executeAction();
			
			$this->fleetObject = $this->objectAction->getReturnValues();
		}
		
		parent::readParameters();
	}
	
	/**
	 * @see	wcf\page\IPage::readData()
	 */
	public function readData() {
		parent::readData();
		
		if (!count($_POST)) {
			$this->schakal = $this->fleetObject->schakal;
			$this->recycler = $this->fleetObject->recycler;
			$this->spionagesonde = $this->fleetObject->spionagesonde;
			$this->renegade = $this->fleetObject->renegade;
			$this->raider = $this->fleetObject->raider;
			$this->falcon = $this->fleetObject->falcon;
			$this->kolonisationsschiff = $this->fleetObject->kolonisationsschiff;
			$this->tjuger = $this->fleetObject->tjuger;
			$this->cougar = $this->fleetObject->cougar;
			$this->longeagleV = $this->fleetObject->longeagleV;
			$this->kleinesHandelsschiff = $this->fleetObject->kleinesHandelsschiff;
			$this->großesHandelsschiff = $this->fleetObject->großesHandelsschiff;
			$this->noah = $this->fleetObject->noah;
			$this->longeagleX = $this->fleetObject->longeagleX;
		}
		
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.index'), LinkHandler::getInstance()->getLink('GwIndex', array(
			'application' => 'gw'
		))));
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->getDynamicVariable('gw.header.planet'), LinkHandler::getInstance()->getLink('PlanetList', array(
			'application' => 'gw'
		))));
	}
	
	/**
	 * @see	wcf\page\IPage::assignVariables()
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		WCF::getTPL()->assign(array(
			'fleetObject' => $this->fleetObject,
			'text' => $this->text,
			'schakal' => $this->schakal,
			'recycler' => $this->recycler,
			'spionagesonde' => $this->spionagesonde,
			'renegade' => $this->renegade,
			'raider' => $this->raider,
			'falcon' => $this->falcon,
			'kolonisationsschiff' => $this->kolonisationsschiff,
			'tjuger' => $this->tjuger,
			'cougar' => $this->cougar,
			'longeagleV' => $this->longeagleV,
			'kleinesHandelsschiff' => $this->kleinesHandelsschiff,
			'großesHandelsschiff' => $this->großesHandelsschiff,
			'noah' => $this->noah,
			'longeagleX' => $this->longeagleX
		));
	}
	
	/**
	 * @see	wcf\form\IForm::readFormParameters()
	 */
	public function readFormParameters() {
		if(isset($_POST['text'])) $this->text = StringUtil::trim($_POST['text']);
		if(isset($_POST['schakal'])) $this->schakal = intval($_POST['schakal']);
		if(isset($_POST['recycler'])) $this->recycler = intval($_POST['recycler']);
		if(isset($_POST['spionagesonde'])) $this->spionagesonde = intval($_POST['spionagesonde']);
		if(isset($_POST['renegade'])) $this->renegade = intval($_POST['renegade']);
		if(isset($_POST['raider'])) $this->raider = intval($_POST['raider']);
		if(isset($_POST['falcon'])) $this->falcon = intval($_POST['falcon']);
		if(isset($_POST['kolonisationsschiff'])) $this->tjuger = intval($_POST['kolonisationsschiff']);
		if(isset($_POST['tjuger'])) $this->tjuger = intval($_POST['tjuger']);
		if(isset($_POST['cougar'])) $this->cougar = intval($_POST['cougar']);
		if(isset($_POST['longeagleV'])) $this->longeagleV = intval($_POST['longeagleV']);
		if(isset($_POST['kleinesHandelsschiff'])) $this->kleinesHandelsschiff = intval($_POST['kleinesHandelsschiff']);
		if(isset($_POST['großesHandelsschiff'])) $this->großesHandelsschiff = intval($_POST['großesHandelsschiff']);
		if(isset($_POST['noah'])) $this->noah = intval($_POST['noah']);
		if(isset($_POST['longeagleX'])) $this->longeagleX = intval($_POST['longeagleX']);
		
		parent::readFormParameters();
	}
	
	/**
	 * @see	wcf\form\IForm::validate()
	 */
	public function validate() {
		parent::validate();
		
		$this->validateText();
	}
	
	public function validateText() {
		if(empty($this->text)) {
			return;
		}
		
		$this->schakal = $this->recycler = $this->spionagesonde = $this->renegade = $this->raider = $this->falcon = $this->tjuger = $this->cougar = 
		$this->longeagleV = $this->kleinesHandelsschiff = $this->großesHandelsschiff = $this->noah = $this->longeagleX = 0;
		
		$delimiter = "\r\n";
		
		$comp = 'Schakal (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->schakal = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Recycler (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->recycler = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Spionagesonde (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->spionagesonde = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Renegade (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->renegade = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Raider (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->raider = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Falcon (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->falcon = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Kolonisationsschiff (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->kolonisationsschiff = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Tjuger (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->tjuger = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Spionagetechnik (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->spionageforschung = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Cougar (';
		$found = strpos($this->text, $comp);
		
		if ($found == true) {
			$this->cougar = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Longeagle V (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->longeagleV = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Kleines Handelsschiff (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->kleinesHandelsschiff = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Großes Handelsschiff (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->großesHandelsschiff = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Noah (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->noah = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Longeagle X (';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->longeagleX = intval(substr($this->text, $found + strlen($comp)));
		}
	}
	
	/**
	 * @see	wcf\form\IForm::save()
	 */
	public function save() {
		parent::save();
		
		// save fleet
		$data = array(
			'username' => WCF::getUser()->username,
			'gwUsername' => WCF::getUser()->username,
			'time' => TIME_NOW,
			'schakal' => $this->schakal,
			'recycler' => $this->recycler,
			'spionagesonde' => $this->spionagesonde,
			'renegade' => $this->renegade,
			'raider' => $this->raider,
			'falcon' => $this->falcon,
			'kolonisationsschiff' => $this->kolonisationsschiff,
			'tjuger' => $this->tjuger,
			'cougar' => $this->cougar,
			'longeagleV' => $this->longeagleV,
			'kleinesHandelsschiff' => $this->kleinesHandelsschiff,
			'großesHandelsschiff' => $this->großesHandelsschiff,
			'noah' => $this->noah,
			'longeagleX' => $this->longeagleX
		);
		
		$fleetData = array(
			'data' => $data
		);
		
		$this->objectAction = new FleetAction(array($this->fleetObject), 'update', $fleetData);
		$this->objectAction->executeAction();
		
		$this->saved();
		
		HeaderUtil::redirect(LinkHandler::getInstance()->getLink('FleetEdit', array(
			'application' => 'gw',
			'object' => $this->fleetObject->getPlanet()
		)));
		exit;
	}
}
