<?php
namespace gw\form;
use gw\data\user\UserAction;
use wcf\form\AbstractForm;
use wcf\system\exception\UserInputException;
use wcf\util\StringUtil;
use wcf\system\breadcrumb\Breadcrumb;
use wcf\system\request\LinkHandler;
use wcf\util\HeaderUtil;
use wcf\system\WCF;

/**
 * Shows the highscore form.
 * 
 * @author	Marcel Beckers
 * @license	YoureCom License - Commercial (YCLC)  <http://yourecom.de/hp/index.php?licence-commercial/>
 * @package	de.yourecom.cbs
 */
class HighscoreForm extends AbstractForm {
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $activeMenuItem = 'gw.header.index';
	
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $loginRequired = true;
	
	/**
	 * @see	wcf\page\AbstractPage::$neededPermissions
	 */
	public $neededPermissions = array('user.gw.general.cannUse');
	
	/**
	 * text
	 * @var	string
	 */
	public $text = '';
	
	/**
	 * @see	wcf\page\IPage::readData()
	 */
	public function readData() {
		parent::readData();
		
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.index'), LinkHandler::getInstance()->getLink('GwIndex', array(
			'application' => 'gw'
		))));
	}
	
	/**
	 * @see	wcf\page\IPage::assignVariables()
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		WCF::getTPL()->assign(array(
			'text' => $this->text
		));
	}
	
	/**
	 * @see	wcf\form\IForm::readFormParameters()
	 */
	public function readFormParameters() {
		if(isset($_POST['text'])) $this->text = StringUtil::trim($_POST['text']);
		
		parent::readFormParameters();
	}
	
	/**
	 * @see	wcf\form\IForm::validate()
	 */
	public function validate() {
		parent::validate();
		
		$this->validateText();
	}
	
	public function validateText() {
		if(empty($this->text)) {
			return;
		}
		
		$this->userData = array();
		
		$text = explode("\r\n", $this->text);
		for($i = 36; $i < count($text); $i++) {
			// next = if in an alliance = 0, if not in an alliance = 1
			$next = 0;
			$textDetail = preg_split("/(	| )/", $text[$i]);
			
			// no place? COntinue!
			if(!is_numeric($textDetail[0])) continue;
			
			$this->userData[$i-36]['gwUsername'] = StringUtil::trim($textDetail[2]);
			
			if(substr($textDetail[3], 0, 1) == '[') {
				$this->userData[$i-36]['alliance'] = $textDetail[3];
			}
			else {
				$this->userData[$i-36]['alliance'] = '';
				$next = 1;
			}
			
			$this->userData[$i-36]['planetPoints'] = intval(str_replace('.', '', $textDetail[5-$next]));
			$this->userData[$i-36]['researchPoints'] = intval(str_replace('.', '', $textDetail[7-$next]));
		}
	}
	
	/**
	 * @see	wcf\form\IForm::save()
	 */
	public function save() {
		parent::save();
		
		foreach($this->userData as $key => $userData) {
			if(!isset($userData['alliance'])) continue;
			
			// save user
			$data = array(
				'userID' => null,
				'alliance' => $userData['alliance'],
				'gwUsername' => $userData['gwUsername'],
				'planetPoints' => $userData['planetPoints'],
				'researchPoints' => $userData ['researchPoints'],
				'time' => TIME_NOW
			);
			
			$userData = array(
				'data' => $data
			);
			
			$this->objectAction = new UserAction(array(), 'create', $userData);
			$resultValues = $this->objectAction->executeAction();
		}
		
		$this->saved();
		
		HeaderUtil::redirect(LinkHandler::getInstance()->getLink('AllUserList', array(
			'application' => 'gw'
		)));
		exit;
	}
}
