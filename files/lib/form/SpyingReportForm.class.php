<?php
namespace gw\form;
use gw\data\fleet\FleetAction;
use gw\data\planet\PlanetAction;
use gw\data\research\ResearchAction;
use gw\data\user\UserAction;
use wcf\form\AbstractForm;
use wcf\system\exception\UserInputException;
use wcf\util\StringUtil;
use wcf\system\breadcrumb\Breadcrumb;
use wcf\system\request\LinkHandler;
use wcf\util\HeaderUtil;
use wcf\system\WCF;

/**
 * Shows the spying report form.
 * 
 * @author	Marcel Beckers
 * @license	YoureCom License - Commercial (YCLC)  <http://yourecom.de/hp/index.php?licence-commercial/>
 * @package	de.yourecom.cbs
 */
class SpyingReportForm extends AbstractForm {
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $activeMenuItem = 'gw.header.index';
	
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $loginRequired = true;
	
	/**
	 * @see	wcf\page\AbstractPage::$neededPermissions
	 */
	public $neededPermissions = array('user.gw.general.cannUse');
	
	/**
	 * text
	 * @var	string
	 */
	public $text = '';
	
	/**
	 * Schakal
	 * @var	int
	 */
	public $schakal = 0;
	
	/**
	 * Recycler
	 * @var	int
	 */
	public $recycler = 0;
	
	/**
	 * Spionagesonde
	 * @var	int
	 */
	public $spionagesonde = 0;
	
	/**
	 * Renegade
	 * @var	int
	 */
	public $renegade = 0;
	
	/**
	 * Raider
	 * @var	int
	 */
	public $raider = 0;
	
	/**
	 * Falcon
	 * @var	int
	 */
	public $falcon = 0;
	
	/**
	 * Kolonisationsschiff
	 * @var	int
	 */
	public $kolonisationsschiff = 0;
	
	/**
	 * Tjuger
	 * @var	int
	 */
	public $tjuger = 0;
	
	/**
	 * Cougar
	 * @var	int
	 */
	public $cougar = 0;
	
	/**
	 * Longeagle V
	 * @var	int
	 */
	public $longeagleV = 0;
	
	/**
	 * Kleines Handelsschiff
	 * @var	int
	 */
	public $kleinesHandelsschiff = 0;
	
	/**
	 * Großes Handelsschiff
	 * @var	int
	 */
	public $großesHandelsschiff = 0;
	
	/**
	 * Noah
	 * @var	int
	 */
	public $noah = 0;
	
	/**
	 * Longeagle X
	 * @var	int
	 */
	public $longeagleX = 0;
	
	/**
	 * Kommandozentrale
	 * @var	int
	 */
	public $kommandozentrale = 1;
	
	/**
	 * Forschungszentrum
	 * @var	int
	 */
	public $forschungszentrum = 0;
	
	/**
	 * Eisenmine
	 * @var	int
	 */
	public $eisenmine = 0;
	
	/**
	 * Lutinumraffinerie
	 * @var	int
	 */
	public $lutinumraffinerie = 0;
	
	/**
	 * Bohrturm
	 * @var	int
	 */
	public $bohrturm = 0;
	
	/**
	 * Chemiefabrik
	 * @var	int
	 */
	public $chemiefabrik = 0;
	
	/**
	 * Erweiterte Chemiefabrik
	 * @var	int
	 */
	public $erweiterteChemiefabrik = 0;
	
	/**
	 * Eisenspeicher
	 * @var	int
	 */
	public $eisenspeicher = 0;
	
	/**
	 * Lutinumspeicher
	 * @var	int
	 */
	public $lutinumspeicher = 0;
	
	/**
	 * Wassertanks
	 * @var	int
	 */
	public $wassertanks = 0;
	
	/**
	 * Wasserstofftank
	 * @var	int
	 */
	public $wasserstofftank = 0;
	
	/**
	 * Schiffsfabrik
	 * @var	int
	 */
	public $schiffsfabrik = 0;
	
	/**
	 * Orbitalen Verteidgungsstation
	 * @var	int
	 */
	public $orbitalenVerteidgungsstation = 0;
	
	/**
	 * Planetarer Schild
	 * @var	int
	 */
	public $planetarerSchild = 0;
	
	/**
	 * Fusionsreaktor
	 * @var	int
	 */
	public $fusionsreaktor = 0;
	
	/**
	 * Verbrennungsantrieb
	 * @var	int
	 */
	public $verbrennungsantrieb = 0;
	
	/**
	 * Ionenantrieb
	 * @var	int
	 */
	public $ionenantrieb = 0;
	
	/**
	 * Raumkrümmungsantrieb
	 * @var	int
	 */
	public $raumkrümmungsantrieb = 0;
	
	/**
	 * Raumfaltungsantrieb
	 * @var	int
	 */
	public $raumfaltungsantrieb = 0;
	
	/**
	 * Ionisation
	 * @var	int
	 */
	public $ionisation = 0;
	
	/**
	 * Energiebündelung
	 * @var	int
	 */
	public $energiebündelung = 0;
	
	/**
	 * Explosivgeschosse
	 * @var	int
	 */
	public $explosivgeschosse = 0;
	
	/**
	 * Spionageforschung
	 * @var	int
	 */
	public $spionageforschung = 0;
	
	/**
	 * Erweiterte Schiffspanzerung
	 * @var	int
	 */
	public $erweiterteSchiffspanzerung = 0;
	
	/**
	 * Erhöhte Ladekapazität
	 * @var	int
	 */
	public $erhöhteLadekapazität = 0;
	
	/**
	 * Recycling-Technik
	 * @var	int
	 */
	public $recyclingTechnik = 0;
	
	/**
	 * @see	wcf\page\IPage::readData()
	 */
	public function readData() {
		parent::readData();
		
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.index'), LinkHandler::getInstance()->getLink('GwIndex', array(
			'application' => 'gw'
		))));
	}
	
	/**
	 * @see	wcf\page\IPage::assignVariables()
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		WCF::getTPL()->assign(array(
			'text' => $this->text
		));
	}
	
	/**
	 * @see	wcf\form\IForm::readFormParameters()
	 */
	public function readFormParameters() {
		if(isset($_POST['text'])) $this->text = StringUtil::trim($_POST['text']);
		
		parent::readFormParameters();
	}
	
	/**
	 * @see	wcf\form\IForm::validate()
	 */
	public function validate() {
		parent::validate();
		
		$this->validateText();
	}
	
	public function validateText() {
		if(empty($this->text)) {
			return;
		}
		
		$this->userData = array();
		
		$text = explode("\r\n", $this->text);
		
		for($i = 3; $i < count($text); $i++) {
			$textDetail = preg_split("/(	| )/", $text[$i]);
			
			// get time
			if($i == 3) {
				$date = explode('.', $textDetail[2]);
				$dateTime = new \DateTime($date[2].'-'.$date[1].'-'.$date[0].' '.$textDetail[4]);
				$this->time = $dateTime->getTimestamp();
			}
			//get coords
			elseif($i == 4) {
				$coords = explode(':', $textDetail[2]);
				$this->universe = intval($coords[0]);
				$this->solarSystem = intval($coords[1]);
				$this->planet = intval($coords[2]);
			}
			elseif($textDetail[0] == 'Schiffe' && $textDetail[4] == 'Verteidigers') { 
				// get username
				$this->username = $textDetail[8];
				
				// get alliance
				$this->alliance = substr(substr($textDetail[9], 1), 0, -1);
				
				// get fleet
				for($i = $i; $i < count($text); $i++) {
					$textDetailFleet = preg_split("/(	| )/", $text[$i]);
					
					if($textDetailFleet[0] == 'Schakal') $this->schakal = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Recycler') $this->recycler = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Spionagesonde') $this->spionagesonde = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Renegade') $this->renegade = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Raider') $this->raider = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Falcon') $this->falcon = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'kolonisationsschiff') $this->kolonisationsschiff = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Tjuger') $this->tjuger = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Cougar') $this->cougar = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Longeagle V') $this->longeagleV = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Kleines Handelsschiff') $this->kleinesHandelsschiff = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Großes Handelsschiff') $this->großesHandelsschiff = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Noah') $this->noah = intval($textDetailFleet[2]);
					if($textDetailFleet[0] == 'Longeagle X') $this->longeagleX = intval($textDetailFleet[2]);
					
					//no more fleet
					if($text[$i] == 'Gesamt' || $text[$i] == 'Keine Schiffe') {
						break;
					}
				}
			}
			// get buildings
			elseif($text[$i] == 'Gebäude') {
				for($i = $i; $i < count($text); $i++) {
					$textDetailBuildings = preg_split("/(	| )/", $text[$i]);
					
					if($textDetailBuildings[0] == 'Kommandozentrale') $this->kommandozentrale = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Forschungszentrum') $this->forschungszentrum = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Eisenmine') $this->eisenmine = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Lutinumraffinerie') $this->lutinumraffinerie = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Bohrturm') $this->bohrturm = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Chemiefabrik') $this->chemiefabrik = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Erweiterte Chemiefabrik') $this->erweiterteChemiefabrik = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Eisenspeicher') $this->eisenspeicher = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Lutinumspeicher') $this->lutinumspeicher = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Wassertanks') $this->wassertanks = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Wasserstofftank') $this->wasserstofftank = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Schiffsfabrik') $this->schiffsfabrik = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Orbitalen Verteidgungsstation') $this->orbitalenVerteidgungsstation = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Planetarer Schild') $this->planetarerSchild = intval($textDetailBuildings[2]);
					if($textDetailBuildings[0] == 'Fusionsreaktor') $this->fusionsreaktor = intval($textDetailBuildings[2]);
					
					//no more buildings
					if($text[$i] == 'Forschungen') {
						break;
					}
				}
			}
			// get research
			elseif($text[$i-1] == 'Forschungen') {
				for($i = $i-1; $i < count($text); $i++) {
					$textDetailResearch = preg_split("/(	| )/", $text[$i]);
					
					if($textDetailResearch[0] == 'Verbrennungsantrieb') $this->verbrennungsantrieb = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Ionenantrieb') $this->ionenantrieb = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Raumkrümmungsantrieb') $this->raumkrümmungsantrieb = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Raumfaltungsantrieb') $this->raumfaltungsantrieb = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Ionisation') $this->ionisation = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Energiebündelung') $this->energiebündelung = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Explosivgeschosse') $this->explosivgeschosse = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Spionageforschung') $this->spionageforschung = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Erweiterte Schiffspanzerung') $this->erweiterteSchiffspanzerung = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Erhöhte Ladekapazität') $this->erhöhteLadekapazität = intval($textDetailResearch[2]);
					if($textDetailResearch[0] == 'Recycling-Technik') $this->recyclingTechnik = intval($textDetailResearch[2]);
					
					//no more researches
					if($text[$i] == 'Rohstoffe 	Im Lager 	Plünderbar') {
						break;
					}
				}
			}
		}
	}
	
	/**
	 * @see	wcf\form\IForm::save()
	 */
	public function save() {
		parent::save();
		
		// save user
		$data = array(
			'userID' => null,
			'alliance' => $this->alliance,
			'gwUsername' => $this->username,
			'planetPoints' => 0,
			'researchPoints' => 0,
			'time' => TIME_NOW
		);
		
		$userData = array(
			'data' => $data
		);
		$this->objectAction = new UserAction(array(), 'create', $userData);
		$resultUserValues = $this->objectAction->executeAction();
		$resultUserValues = $this->objectAction->getReturnValues();
		
		// save planet
		$data = array(
			'username' => '',
			'userID' => null,
			'gwUserID' => $resultUserValues['returnValues']->gwUserID,
			'gwUsername' => $this->username,
			'time' => TIME_NOW,
			'universe' => $this->universe,
			'solarSystem' => $this->solarSystem,
			'planet' => $this->planet,
			'kommandozentrale' => $this->kommandozentrale,
			'forschungszentrum' => $this->forschungszentrum,
			'eisenmine' => $this->eisenmine,
			'lutinumraffinerie' => $this->lutinumraffinerie,
			'bohrturm' => $this->bohrturm,
			'chemiefabrik' => $this->chemiefabrik,
			'erweiterteChemiefabrik' => $this->erweiterteChemiefabrik,
			'eisenspeicher' => $this->eisenspeicher,
			'lutinumspeicher' => $this->lutinumspeicher,
			'wassertanks' => $this->wassertanks,
			'wasserstofftank' => $this->wasserstofftank,
			'schiffsfabrik' => $this->schiffsfabrik,
			'orbitalenVerteidgungsstation' => $this->orbitalenVerteidgungsstation,
			'planetarerSchild' => $this->planetarerSchild,
			'fusionsreaktor' => $this->fusionsreaktor
		);
		
		$objectData = array(
			'data' => $data
		);
		
		$this->objectAction = new PlanetAction(array(), 'create', $objectData);
		$resultPlanetValues = $this->objectAction->executeAction();
		$resultPlanetValues = $this->objectAction->getReturnValues();
		
		// save research
		$data = array(
			'gwUserID' => $resultUserValues['returnValues']->gwUserID,
			'gwUsername' => $this->username,
			'time' => TIME_NOW,
			'verbrennungsantrieb' => $this->verbrennungsantrieb,
			'ionenantrieb' => $this->ionenantrieb,
			'raumkrümmungsantrieb' => $this->raumkrümmungsantrieb,
			'raumfaltungsantrieb' => $this->raumfaltungsantrieb,
			'ionisation' => $this->ionisation,
			'energiebündelung' => $this->energiebündelung,
			'explosivgeschosse' => $this->explosivgeschosse,
			'spionageforschung' => $this->spionageforschung,
			'erweiterteSchiffspanzerung' => $this->erweiterteSchiffspanzerung,
			'erhöhteLadekapazität' => $this->erhöhteLadekapazität,
			'recyclingTechnik' => $this->recyclingTechnik
		);
		
		$objectData = array(
			'data' => $data
		);
		
		$this->objectAction = new ResearchAction(array(), 'create', $objectData);
		$resultResearchValues = $this->objectAction->executeAction();
		$resultResearchValues = $this->objectAction->getReturnValues();
		
		// save fleet
		$data = array(
			'gwUserID' => $resultUserValues['returnValues']->gwUserID,
			'gwUsername' => $this->username,
			'planetID' => $resultPlanetValues['returnValues']->planetID,
			'time' => TIME_NOW,
			'schakal' => $this->schakal,
			'recycler' => $this->recycler,
			'spionagesonde' => $this->spionagesonde,
			'renegade' => $this->renegade,
			'raider' => $this->raider,
			'falcon' => $this->falcon,
			'kolonisationsschiff' => $this->kolonisationsschiff,
			'tjuger' => $this->tjuger,
			'cougar' => $this->cougar,
			'longeagleV' => $this->longeagleV,
			'kleinesHandelsschiff' => $this->kleinesHandelsschiff,
			'großesHandelsschiff' => $this->großesHandelsschiff,
			'noah' => $this->noah,
			'longeagleX' => $this->longeagleX
		);
		
		$objectData = array(
			'data' => $data
		);
		
		$this->objectAction = new FleetAction(array(), 'create', $objectData);
		$resultFleetValues = $this->objectAction->executeAction();
		$resultFleetValues = $this->objectAction->getReturnValues();
		
		$this->saved();
		
		HeaderUtil::redirect(LinkHandler::getInstance()->getLink('AllUserList', array(
			'application' => 'gw'
		)));
		exit;
	}
}
