<?php
namespace gw\form;
use gw\data\planet\Planet;
use gw\data\planet\PlanetAction;
use gw\data\user\User;
use gw\data\user\UserEditor;
use wcf\form\AbstractForm;
use wcf\system\exception\IllegalLinkException;
use wcf\system\exception\UserInputException;
use wcf\util\StringUtil;
use wcf\system\breadcrumb\Breadcrumb;
use wcf\system\request\LinkHandler;
use wcf\util\HeaderUtil;
use wcf\system\WCF;

/**
 * Shows the competition edit form.
 * 
 * @author	Marcel Beckers
 * @license	YoureCom License - Commercial (YCLC)  <http://yourecom.de/hp/index.php?licence-commercial/>
 * @package	de.yourecom.cbs
 */
class PlanetEditForm extends PlanetAddForm {
	/**
	 * planet id
	 * @var	integer
	 */
	public $planetID = 0;
	
	/**
	 * planet object
	 * @var	gw\data\planet\Planet
	 */
	public $planetObject = null;
	
	/**
	 * @see	wcf\page\IPage::readParameters()
	 */
	public function readParameters() {
		if (!empty($_REQUEST['id'])) $this->planetID = intval($_REQUEST['id']);
		$this->planetObject = new Planet($this->planetID);
		if (!$this->planetObject->planetID) {
			throw new IllegalLinkException();
		}
		
		parent::readParameters();
	}
	
	/**
	 * @see	wcf\page\IPage::readData()
	 */
	public function readData() {
		parent::readData();
		
		if (!count($_POST)) {
			$this->universe = $this->planetObject->universe;
			$this->solarSystem = $this->planetObject->solarSystem;
			$this->planet = $this->planetObject->planet;
			$this->kommandozentrale = $this->planetObject->kommandozentrale;
			$this->forschungszentrum = $this->planetObject->forschungszentrum;
			$this->eisenmine = $this->planetObject->eisenmine;
			$this->lutinumraffinerie = $this->planetObject->lutinumraffinerie;
			$this->bohrturm = $this->planetObject->bohrturm;
			$this->chemiefabrik = $this->planetObject->chemiefabrik;
			$this->erweiterteChemiefabrik = $this->planetObject->erweiterteChemiefabrik;
			$this->eisenspeicher = $this->planetObject->eisenspeicher;
			$this->lutinumspeicher = $this->planetObject->lutinumspeicher;
			$this->wassertanks = $this->planetObject->wassertanks;
			$this->wasserstofftank = $this->planetObject->wasserstofftank;
			$this->schiffsfabrik = $this->planetObject->schiffsfabrik;
			$this->orbitalenVerteidgungsstation = $this->planetObject->orbitalenVerteidgungsstation;
			$this->planetarerSchild = $this->planetObject->planetarerSchild;
			$this->fusionsreaktor = $this->planetObject->fusionsreaktor;
		}
		
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.index'), LinkHandler::getInstance()->getLink('GwIndex', array(
			'application' => 'gw'
		))));
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->getDynamicVariable('gw.header.planet'), LinkHandler::getInstance()->getLink('PlanetList', array(
			'application' => 'gw'
		))));
	}
	
	/**
	 * @see	wcf\page\IPage::assignVariables()
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		WCF::getTPL()->assign(array(
			'action' => 'edit',
			'planetObject' => $this->planetObject,
			'planetID' => $this->planetID,
			'kommandozentrale' => $this->kommandozentrale,
			'forschungszentrum' => $this->forschungszentrum,
			'eisenmine' => $this->eisenmine,
			'lutinumraffinerie' => $this->lutinumraffinerie,
			'bohrturm' => $this->bohrturm,
			'chemiefabrik' => $this->chemiefabrik,
			'erweiterteChemiefabrik' => $this->erweiterteChemiefabrik,
			'eisenspeicher' => $this->eisenspeicher,
			'lutinumspeicher' => $this->lutinumspeicher,
			'wassertanks' => $this->wassertanks,
			'wasserstofftank' => $this->wasserstofftank,
			'schiffsfabrik' => $this->schiffsfabrik,
			'orbitalenVerteidgungsstation' => $this->orbitalenVerteidgungsstation,
			'planetarerSchild' => $this->planetarerSchild,
			'fusionsreaktor' => $this->fusionsreaktor
		));
	}
	
	/**
	 * @see	wcf\form\IForm::save()
	 */
	public function save() {
		AbstractForm::save();
		
		// save planet
		$data = array(
			'gwUserID' => WCF::getUser()->userID,
			'username' => WCF::getUser()->username,
			'gwUsername' => WCF::getUser()->username,
			'points' => $this->calculatePlanetPoints(),
			'time' => TIME_NOW,
			'universe' => $this->universe,
			'solarSystem' => $this->solarSystem,
			'planet' => $this->planet,
			'kommandozentrale' => $this->kommandozentrale,
			'forschungszentrum' => $this->forschungszentrum,
			'eisenmine' => $this->eisenmine,
			'lutinumraffinerie' => $this->lutinumraffinerie,
			'bohrturm' => $this->bohrturm,
			'chemiefabrik' => $this->chemiefabrik,
			'erweiterteChemiefabrik' => $this->erweiterteChemiefabrik,
			'eisenspeicher' => $this->eisenspeicher,
			'lutinumspeicher' => $this->lutinumspeicher,
			'wassertanks' => $this->wassertanks,
			'wasserstofftank' => $this->wasserstofftank,
			'schiffsfabrik' => $this->schiffsfabrik,
			'orbitalenVerteidgungsstation' => $this->orbitalenVerteidgungsstation,
			'planetarerSchild' => $this->planetarerSchild,
			'fusionsreaktor' => $this->fusionsreaktor
		);
		
		$planetData = array(
			'data' => $data
		);
		
		$this->objectAction = new PlanetAction(array($this->planetObject), 'update', $planetData);
		$this->objectAction->executeAction();
		
		//save planet points
		$user = new User(WCF::getUser()->userID);
		$userEditor = new UserEditor($user);
		$userEditor->update(array(
			'planetPoints' => round($this->calculateAllPlanetPoints())
		));
		
		$this->saved();
		
		HeaderUtil::redirect(LinkHandler::getInstance()->getLink('PlanetList', array(
			'application' => 'gw'
		)));
		exit;
	}
}
