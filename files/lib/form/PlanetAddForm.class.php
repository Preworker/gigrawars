<?php
namespace gw\form;
use gw\data\planet\PlanetAction;
use gw\data\user\User;
use gw\data\user\UserEditor;
use wcf\form\AbstractForm;
use wcf\system\exception\UserInputException;
use wcf\util\StringUtil;
use wcf\system\breadcrumb\Breadcrumb;
use wcf\system\request\LinkHandler;
use wcf\util\HeaderUtil;
use wcf\system\WCF;

/**
 * Shows the new  form.
 * 
 * @author	Marcel Beckers
 * @license	YoureCom License - Commercial (YCLC)  <http://yourecom.de/hp/index.php?licence-commercial/>
 * @package	de.yourecom.gw
 */
class PlanetAddForm extends AbstractForm {
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $activeMenuItem = 'gw.header.index';
	
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $loginRequired = true;
	
	/**
	 * @see	wcf\page\AbstractPage::$neededPermissions
	 */
	public $neededPermissions = array('user.gw.general.cannUse');
	
	/**
	 * text
	 * @var	string
	 */
	public $text = '';
	
	/**
	 * universe
	 * @var	integer
	 */
	public $universe = 1;
	
	/**
	 * solarSystem
	 * @var	integer
	 */
	public $solarSystem = 1;
	
	/**
	 * planet
	 * @var	integer
	 */
	public $planet = 1;
	
	/**
	 * @see	wcf\page\IPage::readData()
	 */
	public function readData() {
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.index'), LinkHandler::getInstance()->getLink('GwIndex', array(
			'application' => 'gw'
		))));
		parent::readData();
	}
	
	/**
	 * @see	wcf\page\IPage::assignVariables()
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		WCF::getTPL()->assign(array(
			'action' => 'add',
			'universe' => $this->universe,
			'solarSystem' => $this->solarSystem,
			'planet' => $this->planet,
			'text' => $this->text
		));
	}
	
	/**
	 * @see	wcf\form\IForm::readFormParameters()
	 */
	public function readFormParameters() {
		if(isset($_POST['universe'])) $this->universe = intval($_POST['universe']);
		if(isset($_POST['solarSystem'])) $this->solarSystem = intval($_POST['solarSystem']);
		if(isset($_POST['planet'])) $this->planet = intval($_POST['planet']);
		if(isset($_POST['text'])) $this->text = StringUtil::trim($_POST['text']);
		if(isset($_POST['kommandozentrale'])) $this->kommandozentrale = intval($_POST['kommandozentrale']);
		if(isset($_POST['forschungszentrum'])) $this->forschungszentrum = intval($_POST['forschungszentrum']);
		if(isset($_POST['eisenmine'])) $this->eisenmine = intval($_POST['eisenmine']);
		if(isset($_POST['lutinumraffinerie'])) $this->lutinumraffinerie = intval($_POST['lutinumraffinerie']);
		if(isset($_POST['bohrturm'])) $this->bohrturm = intval($_POST['bohrturm']);
		if(isset($_POST['chemiefabrik'])) $this->chemiefabrik = intval($_POST['chemiefabrik']);
		if(isset($_POST['erweiterteChemiefabrik'])) $this->erweiterteChemiefabrik = intval($_POST['erweiterteChemiefabrik']);
		if(isset($_POST['eisenspeicher'])) $this->eisenspeicher = intval($_POST['eisenspeicher']);
		if(isset($_POST['lutinumspeicher'])) $this->lutinumspeicher = intval($_POST['lutinumspeicher']);
		if(isset($_POST['wassertanks'])) $this->wassertanks = intval($_POST['wassertanks']);
		if(isset($_POST['wasserstofftank'])) $this->wasserstofftank = intval($_POST['wasserstofftank']);
		if(isset($_POST['schiffsfabrik'])) $this->schiffsfabrik = intval($_POST['schiffsfabrik']);
		if(isset($_POST['orbitalenVerteidgungsstation'])) $this->orbitalenVerteidgungsstation = intval($_POST['orbitalenVerteidgungsstation']);
		if(isset($_POST['planetarerSchild'])) $this->planetarerSchild = intval($_POST['planetarerSchild']);
		if(isset($_POST['fusionsreaktor'])) $this->fusionsreaktor = intval($_POST['fusionsreaktor']);
		
		parent::readFormParameters();
	}
	
	/**
	 * @see	wcf\form\IForm::validate()
	 */
	public function validate() {
		parent::validate();
		
		if(empty($this->universe) || $this->universe < 1 || $this->universe > 200) {
			throw new UserInputException('universe');
		}
		
		if(empty($this->solarSystem) || $this->solarSystem < 1 || $this->solarSystem > 200) {
			throw new UserInputException('solarSystem');
		}
		
		if(empty($this->planet) || $this->planet < 1 || $this->planet > 20) {
			throw new UserInputException('planet');
		}
		
		$this->validateText();
	}
	
	public function validateText() {
		if(empty($this->text)) {
			return;
		}
		
		$this->kommandozentrale = 1;
		
		$this->forschungszentrum = $this->eisenmine = $this->lutinumraffinerie = $this->bohrturm = $this->chemiefabrik = $this->erweiterteChemiefabrik = 
		$this->eisenspeicher = $this->lutinumspeicher = $this->wassertanks = $this->wasserstofftank = $this->schiffsfabrik =
		$this->orbitalenVerteidgungsstation = $this->planetarerSchild = $this->fusionsreaktor = 0;
		
		$delimiter = "\r\n";
		
		$comp = 'Kommandozentrale Stufe ';
		$found = strpos($this->text, $comp);
		
		if ($found === false) {
			throw new UserInputException('text');
		}
		else {
			$this->kommandozentrale = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Forschungszentrum Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->forschungszentrum = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Eisenmine Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->eisenmine = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Lutinumraffinerie Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->lutinumraffinerie = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Bohrturm Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->bohrturm = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Chemiefabrik Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->chemiefabrik = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Erweiterte Chemiefabrik Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->erweiterteChemiefabrik = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Eisenspeicher Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->eisenspeicher = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Lutinumspeicher Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->lutinumspeicher = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Wassertanks Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->wassertanks = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Wasserstofftanks Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->wasserstofftank = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Schiffsfabrik Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->schiffsfabrik = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Orbitalen Verteidgungsstation Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->orbitalenVerteidgungsstation = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Planetarer Schild Stufe ';
		$found = strpos($this->text, $comp);
		if ($found === true) {
			$this->planetarerSchild = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Fusionsreaktor Stufe ';
		$found = strpos($this->text, $comp);
		if ($found === true) {
			$this->fusionsreaktor = intval(substr($this->text, $found + strlen($comp)));
		}
	}
	
	/**
	 * @see	wcf\form\IForm::save()
	 */
	public function save() {
		parent::save();
		
		// save planet
		$data = array(
			'username' => WCF::getUser()->username,
			'gwUserID' => WCF::getUser()->userID,
			'gwUsername' => WCF::getUser()->username,
			'points' => $this->calculatePlanetPoints(),
			'time' => TIME_NOW,
			'universe' => $this->universe,
			'solarSystem' => $this->solarSystem,
			'planet' => $this->planet,
			'kommandozentrale' => $this->kommandozentrale,
			'forschungszentrum' => $this->forschungszentrum,
			'eisenmine' => $this->eisenmine,
			'lutinumraffinerie' => $this->lutinumraffinerie,
			'bohrturm' => $this->bohrturm,
			'chemiefabrik' => $this->chemiefabrik,
			'erweiterteChemiefabrik' => $this->erweiterteChemiefabrik,
			'eisenspeicher' => $this->eisenspeicher,
			'lutinumspeicher' => $this->lutinumspeicher,
			'wassertanks' => $this->wassertanks,
			'wasserstofftank' => $this->wasserstofftank,
			'schiffsfabrik' => $this->schiffsfabrik,
			'orbitalenVerteidgungsstation' => $this->orbitalenVerteidgungsstation,
			'planetarerSchild' => $this->planetarerSchild,
			'fusionsreaktor' => $this->fusionsreaktor
		);
		
		$objectData = array(
			'data' => $data
		);
		
		$this->objectAction = new PlanetAction(array(), 'create', $objectData);
		$resultValues = $this->objectAction->executeAction();
		$returnValues = $this->objectAction->getReturnValues();
		
		//save planet points
		$user = new User(WCF::getUser()->userID);
		$userEditor = new UserEditor($user);
		$userEditor->update(array(
			'planetPoints' => round($this->calculateAllPlanetPoints())
		));
		
		$this->saved();
		
		HeaderUtil::redirect(LinkHandler::getInstance()->getLink('PlanetList', array(
			'application' => 'gw'
		)));
		exit;
	}
	
	public function calculateAllPlanetPoints() {
		$points = 0;
		
		$sql = "SELECT	*
				FROM	gw".WCF_N."_planets
				WHERE	gwUserID = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(
			array(WCF::getUser()->userID)
		);
		while ($row = $statement->fetchArray()) {
			
			$points += $row['kommandozentrale'] * 1.75 + $row['forschungszentrum'] * 1 + $row['eisenmine'] * 0.675 + $row['lutinumraffinerie'] * 0.375
				+ $row['bohrturm'] * 0.3 + $row['chemiefabrik'] * 0.925 + $row['erweiterteChemiefabrik'] * 90 + $row['eisenspeicher'] * 10
				+ $row['lutinumspeicher'] * 10 + $row['wassertanks'] * 5 + $row['wasserstofftank'] * 10 + $row['schiffsfabrik'] * 15.05
				+ $row['orbitalenVerteidgungsstation'] * 2.05 + $row['planetarerSchild'] * 55 + $row['fusionsreaktor'] * 55;
		}
		
		return $points;
	}
	
	public function calculatePlanetPoints() {
		return $this->kommandozentrale * 1.75 + $this->forschungszentrum * 1 + $this->eisenmine * 0.675 + $this->lutinumraffinerie * 0.375
				+ $this->bohrturm * 0.3 + $this->chemiefabrik * 0.925 + $this->erweiterteChemiefabrik * 90 + $this->eisenspeicher * 10
				+ $this->lutinumspeicher * 10 + $this->wassertanks * 5 + $this->wasserstofftank * 10 + $this->schiffsfabrik * 15.05
				+ $this->orbitalenVerteidgungsstation * 2.05 + $this->planetarerSchild * 55 + $this->fusionsreaktor * 55;
	}
}
