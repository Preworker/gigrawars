<?php
namespace gw\form;
use gw\data\research\Research;
use gw\data\research\ResearchAction;
use gw\data\user\User;
use gw\data\user\UserEditor;
use gw\data\user\UserAction;
use wcf\form\AbstractForm;
use wcf\system\exception\UserInputException;
use wcf\util\StringUtil;
use wcf\system\breadcrumb\Breadcrumb;
use wcf\system\request\LinkHandler;
use wcf\util\HeaderUtil;
use wcf\system\WCF;

/**
 * Shows the research edit form.
 * 
 * @author	Marcel Beckers
 * @license	YoureCom License - Commercial (YCLC)  <http://yourecom.de/hp/index.php?licence-commercial/>
 * @package	de.yourecom.cbs
 */
class ResearchEditForm extends AbstractForm {
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $activeMenuItem = 'gw.header.index';
	
	/**
	 * @see	wcf\page\AbstractPage::$activeMenuItem
	 */
	public $loginRequired = true;
	
	/**
	 * @see	wcf\page\AbstractPage::$neededPermissions
	 */
	public $neededPermissions = array('user.gw.general.cannUse');
	
	/**
	 * name of the template for the called page
	 * @var	string
	 */
	public $templateName = 'researchEdit';
	
	/**
	 * user ID
	 * @var	integer
	 */
	public $userID = 0;
	
	/**
	 * research object
	 * @var	gw\data\research\Research
	 */
	public $researchObject = null;
	
	/**
	 * text
	 * @var	string
	 */
	public $text = '';
	
	/**
	 * @see	wcf\page\IPage::readParameters()
	 */
	public function readParameters() {
		if (!empty($_REQUEST['id'])) $this->gwUserID = intval($_REQUEST['id']);
		else {
			$gwUser = User::checkUserID(WCF::getUser()->userID);
			
			if($gwUser === null) {
				$data = array(
					'userID' => WCF::getUser()->userID,
					'gwUserID' => WCF::getUser()->userID,
					'username' => WCF::getUser()->username,
					'gwUsername' => WCF::getUser()->username,
					'alliance' => GW_ALLIANCE_TAG
				);
				
				$objectData = array(
					'data' => $data
				);
				
				$objectAction = new UserAction(array(), 'create', $objectData);
				$resultValues = $objectAction->executeAction();
				$returnValues = $objectAction->getReturnValues();
				
				$this->gwUserID = $returnValues['returnValues']->gwUserID;
			}
			else {
				$this->gwUserID = $gwUser->gwUserID;
			}
		}
		
		$this->researchObject = new Research(null, null, null, $this->gwUserID);
		
		//if dont exist a row create a new one
		if (!$this->researchObject->researchID) {
			$data = array(
				'gwUserID' => $this->gwUserID,
				'username' => WCF::getUser()->username,
				'gwUsername' => WCF::getUser()->username,
			);
			
			$objectData = array(
				'data' => $data
			);
			
			$this->objectAction = new ResearchAction(array(), 'create', $objectData);
			$resultValues = $this->objectAction->executeAction();
			
			$this->researchObject = $this->objectAction->getReturnValues()['returnValues'];
		}
		
		parent::readParameters();
	}
	
	/**
	 * @see	wcf\page\IPage::readData()
	 */
	public function readData() {
		parent::readData();
		
		if (!count($_POST)) {
			$this->verbrennungsantrieb = $this->researchObject->verbrennungsantrieb;
			$this->ionenantrieb = $this->researchObject->ionenantrieb;
			$this->raumkrümmungsantrieb = $this->researchObject->raumkrümmungsantrieb;
			$this->raumfaltungsantrieb = $this->researchObject->raumfaltungsantrieb;
			$this->ionisation = $this->researchObject->ionisation;
			$this->energiebündelung = $this->researchObject->energiebündelung;
			$this->explosivgeschosse = $this->researchObject->explosivgeschosse;
			$this->spionageforschung = $this->researchObject->spionageforschung;
			$this->erweiterteSchiffspanzerung = $this->researchObject->erweiterteSchiffspanzerung;
			$this->erhöhteLadekapazität = $this->researchObject->erhöhteLadekapazität;
			$this->recyclingTechnik = $this->researchObject->recyclingTechnik;
		}
		
		// add breadcrumbs
		WCF::getBreadcrumbs()->add(new Breadcrumb(WCF::getLanguage()->get('gw.header.index'), LinkHandler::getInstance()->getLink('GwIndex', array(
			'application' => 'gw'
		))));
	}
	
	/**
	 * @see	wcf\page\IPage::assignVariables()
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		WCF::getTPL()->assign(array(
			'researchObject' => $this->researchObject,
			'text' => $this->text,
			'verbrennungsantrieb' => $this->verbrennungsantrieb,
			'ionenantrieb' => $this->ionenantrieb,
			'raumkrümmungsantrieb' => $this->raumkrümmungsantrieb,
			'raumfaltungsantrieb' => $this->raumfaltungsantrieb,
			'ionisation' => $this->ionisation,
			'energiebündelung' => $this->energiebündelung,
			'explosivgeschosse' => $this->explosivgeschosse,
			'spionageforschung' => $this->spionageforschung,
			'erweiterteSchiffspanzerung' => $this->erweiterteSchiffspanzerung,
			'erhöhteLadekapazität' => $this->erhöhteLadekapazität,
			'recyclingTechnik' => $this->recyclingTechnik
		));
	}
	
	/**
	 * @see	wcf\form\IForm::readFormParameters()
	 */
	public function readFormParameters() {
		if(isset($_POST['text'])) $this->text = StringUtil::trim($_POST['text']);
		if(isset($_POST['verbrennungsantrieb'])) $this->verbrennungsantrieb = intval($_POST['verbrennungsantrieb']);
		if(isset($_POST['ionenantrieb'])) $this->ionenantrieb = intval($_POST['ionenantrieb']);
		if(isset($_POST['raumkrümmungsantrieb'])) $this->raumkrümmungsantrieb = intval($_POST['raumkrümmungsantrieb']);
		if(isset($_POST['raumfaltungsantrieb'])) $this->raumfaltungsantrieb = intval($_POST['raumfaltungsantrieb']);
		if(isset($_POST['ionisation'])) $this->ionisation = intval($_POST['ionisation']);
		if(isset($_POST['energiebündelung'])) $this->energiebündelung = intval($_POST['energiebündelung']);
		if(isset($_POST['explosivgeschosse'])) $this->explosivgeschosse = intval($_POST['explosivgeschosse']);
		if(isset($_POST['spionageforschung'])) $this->spionageforschung = intval($_POST['spionageforschung']);
		if(isset($_POST['erweiterteSchiffspanzerung'])) $this->erweiterteSchiffspanzerung = intval($_POST['erweiterteSchiffspanzerung']);
		if(isset($_POST['erhöhteLadekapazität'])) $this->erhöhteLadekapazität = intval($_POST['erhöhteLadekapazität']);
		if(isset($_POST['recyclingTechnik'])) $this->recyclingTechnik = intval($_POST['recyclingTechnik']);
		
		parent::readFormParameters();
	}
	
	/**
	 * @see	wcf\form\IForm::validate()
	 */
	public function validate() {
		parent::validate();
		
		$this->validateText();
	}
	
	public function validateText() {
		if(empty($this->text)) {
			return;
		}
		
		$this->verbrennungsantrieb = $this->ionenantrieb = $this->raumkrümmungsantrieb = $this->raumfaltungsantrieb = $this->ionisation =
		$this->energiebündelung = $this->explosivgeschosse = $this->spionageforschung = $this->erweiterteSchiffspanzerung = 
		$this->erhöhteLadekapazität = $this->recyclingTechnik = 0;
		
		$delimiter = "\r\n";
		
		$comp = 'Verbrennungsantrieb Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->verbrennungsantrieb = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Ionenantrieb Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->ionenantrieb = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Raumkrümmungsantrieb Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->raumkrümmungsantrieb = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Raumfaltungsantrieb Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->raumfaltungsantrieb = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Ionisation Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->ionisation = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Energiebündelung Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->energiebündelung = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Explosivgeschosse Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->explosivgeschosse = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Spionagetechnik Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->spionageforschung = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Erweiterte Schiffspanzerung Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->erweiterteSchiffspanzerung = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Erhöhte Ladekapazität Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->erhöhteLadekapazität = intval(substr($this->text, $found + strlen($comp)));
		}
		
		$comp = 'Recycling-Technik Stufe ';
		$found = strpos($this->text, $comp);
		if ($found == true) {
			$this->recyclingTechnik = intval(substr($this->text, $found + strlen($comp)));
		}
	}
	
	/**
	 * @see	wcf\form\IForm::save()
	 */
	public function save() {
		AbstractForm::save();
		
		// save research
		$data = array(
			'gwUserID' => $this->researchObject->gwUserID,
			'username' => WCF::getUser()->username,
			'gwUsername' => WCF::getUser()->username,
			'time' => TIME_NOW,
			'verbrennungsantrieb' => $this->verbrennungsantrieb,
			'ionenantrieb' => $this->ionenantrieb,
			'raumkrümmungsantrieb' => $this->raumkrümmungsantrieb,
			'raumfaltungsantrieb' => $this->raumfaltungsantrieb,
			'ionisation' => $this->ionisation,
			'energiebündelung' => $this->energiebündelung,
			'explosivgeschosse' => $this->explosivgeschosse,
			'spionageforschung' => $this->spionageforschung,
			'erweiterteSchiffspanzerung' => $this->erweiterteSchiffspanzerung,
			'erhöhteLadekapazität' => $this->erhöhteLadekapazität,
			'recyclingTechnik' => $this->recyclingTechnik
		);
		
		$researchData = array(
			'data' => $data
		);
		
		$this->objectAction = new ResearchAction(array($this->researchObject), 'update', $researchData);
		$this->objectAction->executeAction();
		
		//save research points
		$user = new User($this->researchObject->gwUserID);
		$userEditor = new UserEditor($user);
		$userEditor->update(array(
			'researchPoints' => round($this->calculateResearchPoints())
		));
		
		$this->saved();
		
		HeaderUtil::redirect(LinkHandler::getInstance()->getLink('ResearchEdit', array(
			'application' => 'gw',
			'id' => $this->researchObject->gwUserID
		)));
		exit;
	}
	
	public function calculateResearchPoints() {
		return $this->verbrennungsantrieb * 2.5 + $this->ionenantrieb * 25 + $this->raumkrümmungsantrieb * 75 +
			$this->raumfaltungsantrieb * 300 + $this->ionisation * 39.25 + $this->energiebündelung * 78.725 + $this->explosivgeschosse * 10 +
			$this->spionageforschung * 1.25 + $this->erweiterteSchiffspanzerung * 40 + $this->erhöhteLadekapazität * 25.5 +
			$this->recyclingTechnik * 72.5;
	}
}
